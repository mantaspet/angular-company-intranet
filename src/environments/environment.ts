// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false
};

export const apiPath = 'http://company.intranet/api';
export const authPath = 'http://company.intranet/oauth/token';
export const grant_type = 'password';
export const client_id = 4;
export const client_secret = 'FnPXtKJtoanKSP4BgvSB7fE1dFdPYmR4h0lJ7ban';
export const whitelistedDomains = ['http://company.intranet'];
