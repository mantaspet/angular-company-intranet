import { HolidayHistoryService } from './../../employees/holiday-history/holiday-history.service';
import { EmployeeHistoryService } from './../../employees/employee-history/employee-history.service';
import { AssetService } from './../../assets/asset.service';
import { AssetAssignmentService } from './../../assets/asset-assignments/asset-assignment.service';
import { MatMenuTrigger, MatMenuPanel } from '@angular/material/menu';
import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { AuthenticationService } from '../../authentication/authentication.service';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { HolidayHistoryEditComponent } from '../../employees/holiday-history/holiday-history-edit/holiday-history-edit.component';
import { AssetAssignmentEditComponent } from '../../assets/asset-assignments/asset-assignment-edit/asset-assignment-edit.component';
import { SalaryHistoryService } from '../../employees/salary-history/salary-history.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  name: string;
  role: string;
  @ViewChild(MatMenuTrigger) trigger: MatMenuTrigger;

  constructor(
    private authService: AuthenticationService,
    private employeeHistoryService: EmployeeHistoryService,
    private salaryHistoryService: SalaryHistoryService,
    private holidayHistoryService: HolidayHistoryService,
    private router: Router,
    private assetAssignmentService: AssetAssignmentService,
    private assetService: AssetService,
    public dialog: MatDialog) { }

  ngOnInit() {
    this.role = localStorage.getItem('role');
    this.name = localStorage.getItem('name');
  }

  onGetAssetAssignments() {
    this.assetAssignmentService.assetAssignmentsChanged.next([]);
    this.router.navigate(['asset_assignments']);
  }

  onGetAssetRequests() {
    this.assetAssignmentService.assetAssignmentsChanged.next([]);
    this.router.navigate(['asset_assignments', { page: 'manage_requests' }]);
  }

  onGetMyEmploymentHistory() {
    this.employeeHistoryService.employeeHistoryChanged.next([]);
    this.router.navigate(['employee_history', { page: 'my_history' }]);
  }

  onGetMySalaryHistory() {
    this.employeeHistoryService.employeeHistoryChanged.next([]);
    this.router.navigate(['salary_history', { page: 'my_history' }]);
  }

  onGetMyHolidayHistory() {
    this.employeeHistoryService.employeeHistoryChanged.next([]);
    this.router.navigate(['holiday_history', { page: 'my_history' }]);
  }

  onGetMyRequests() {
    this.assetAssignmentService.assetAssignmentsChanged.next([]);
    this.router.navigate(['asset_assignments', { page: 'my_requests' }]);
  }

  onGetMyAssets() {
    this.assetAssignmentService.assetAssignmentsChanged.next([]);
    this.router.navigate(['asset_assignments', { page: 'my_assets' }]);
  }

  // onNewHolidayRequest() {
  //   const employeeHistoryDialog = this.dialog.open(HolidayHistoryEditComponent, {
  //     data: {
  //       editMode: false,
  //       employee_id: undefined,
  //       employee_holiday_history: {
  //         type: '',
  //         from: '',
  //         to: '',
  //         responsible_employee: {
  //           contact: {
  //             first_name: '',
  //             last_name: ''
  //           }
  //         }
  //       }
  //     }
  //   });
  // }

  // onNewAssetRequest() {
  //   const assetRequestDialog = this.dialog.open(AssetAssignmentEditComponent, {
  //     data: {
  //       editMode: false,
  //       page: 'my_requests',
  //       asset_assignment: {
  //         type: '',
  //         deadline: '',
  //         request_message: '',
  //         is_confirmed: null
  //       }
  //     }
  //   });
  // }

  onSignOut() {
    this.authService.signoutUser();
  }
}
