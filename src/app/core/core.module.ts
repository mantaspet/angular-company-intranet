import { CommonModule } from '@angular/common';
import { AssetAssignmentEditComponent } from './../assets/asset-assignments/asset-assignment-edit/asset-assignment-edit.component';
import { NgModule } from '@angular/core';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { MatSidenavModule, MatSnackBarModule, MatDialogModule, MatTooltipModule } from '@angular/material';
import { MatMenuModule } from '@angular/material/menu';
import { MatButtonModule } from '@angular/material';
import { MatToolbarModule } from '@angular/material';
import { MatIconModule } from '@angular/material';
import { HolidayHistoryEditComponent } from '../employees/holiday-history/holiday-history-edit/holiday-history-edit.component';

@NgModule({
  declarations: [
    HeaderComponent,
    HomeComponent
  ],
  imports: [
    CommonModule,
    MatSidenavModule,
    MatMenuModule,
    MatButtonModule,
    MatToolbarModule,
    MatIconModule,
    MatSnackBarModule,
    MatDialogModule,
    MatTooltipModule
  ],
  exports: [
    HeaderComponent
  ],
  entryComponents: [
    HolidayHistoryEditComponent,
    AssetAssignmentEditComponent
  ]
})
export class CoreModule { }
