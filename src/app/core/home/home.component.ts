import { Component, OnInit, ViewChild } from '@angular/core';
import { MatMenuTrigger } from '@angular/material/menu';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  @ViewChild(MatMenuTrigger) trigger: MatMenuTrigger;

  constructor() { }

  ngOnInit() {
  }

  onShowMenu() {
    this.trigger.openMenu();
    console.log('Button clicked!');
  }

  onHideMenu() {
    this.trigger.closeMenu();
    console.log('Button clicked!');
  }
}
