import { Component, OnInit } from '@angular/core';
import { DataSource } from '@angular/cdk/collections';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import { Position } from '../position.model';
import { PositionService } from '../position.service';
import { MatDialog } from '@angular/material';
import { PositionEditComponent } from '../position-edit/position-edit.component';

let positionObservable;

@Component({
  selector: 'app-position-list',
  styleUrls: ['position-list.component.css'],
  templateUrl: 'position-list.component.html',
})
export class PositionListComponent implements OnInit {
  displayedColumns = ['name']; // 'delete'
  dataSource = new PositionDataSource();
  loading = false;

  constructor(
    private positionService: PositionService,
    public dialog: MatDialog) {}

  ngOnInit() {
    this.positionService.loading.subscribe(
      (status: boolean) => {
        this.loading = status;
      }
    );
    this.positionService.getPositions();
    positionObservable = this.positionService.positionsChanged;
  }

  onNewPosition(): void {
    const positionDialog = this.dialog.open(PositionEditComponent, {
      data: {
        editMode: false,
        position: {
          name: '',
          country: '',
          city: '',
          address: ''
        }
      }
    });
  }

  onUpdatePosition(position: Position) {
    const positionDialog = this.dialog.open(PositionEditComponent, {
      data: {
        editMode: true,
        position: position
      }
    });
  }

  // onDeletePosition(position, event) {
  //   event.stopPropagation();
  //   const confirmDialog = this.dialog.open(ConfirmBoxComponent, {
  //     data: {
  //       type: 'pareigas'
  //     }
  //   });
  //   confirmDialog.afterClosed().subscribe(confirmed => {
  //     if (confirmed) {
  //       this.positionService.deletePosition(position);
  //     }
  //   });
  // }
}

export class PositionDataSource extends DataSource<any> {
  connect(): Observable<Position[]> {
    return positionObservable;
  }

  disconnect() {}
}
