import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PositionEditComponent } from './position-edit/position-edit.component';
import { FormsModule } from '@angular/forms';
import { PositionService } from './position.service';
import { PositionListComponent } from './position-list/position-list.component';
import {
  MatTableModule, MatToolbarModule, MatButtonModule, MatInputModule, MatFormFieldModule, MatCardModule, MatDialogModule,
  MatSnackBarModule, MatIconModule, MatTooltipModule, MatProgressBarModule
} from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatCardModule,
    MatButtonModule,
    MatTableModule,
    MatToolbarModule,
    MatDialogModule,
    MatSnackBarModule,
    MatIconModule,
    MatTooltipModule,
    MatProgressBarModule
  ],
  declarations: [PositionEditComponent, PositionListComponent],
  providers: [PositionService],
  entryComponents: [PositionEditComponent]
})
export class PositionsModule { }
