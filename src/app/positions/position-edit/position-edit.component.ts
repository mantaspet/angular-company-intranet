import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { PositionService } from '../position.service';
import { MatDialogRef } from '@angular/material';
import { MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-position-edit',
  templateUrl: './position-edit.component.html',
  styleUrls: ['./position-edit.component.css']
})
export class PositionEditComponent implements OnInit {
  editMode = false;
  wasChanged = false;
  @ViewChild('f') positionForm: NgForm;

  constructor(
    private positionService: PositionService,
    public dialogRef: MatDialogRef<PositionEditComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
    if (this.data.editMode) {
      this.editMode = this.data.editMode;
      const tmpPosition = {...this.data.position};
      this.dialogRef.beforeClose().subscribe(
        () => {
          if (!this.wasChanged) {
            this.positionForm.setValue({
              name: tmpPosition.name
            });
          }
        }
      );
    }
  }

  onSubmitPosition(form: NgForm, position: any) {
    if (this.editMode) {
      this.positionService.updatePosition(position);
      this.wasChanged = true;
    } else {
      this.positionService.addPosition(form.value);
      this.wasChanged = true;
    }
    this.editMode = false;
    this.dialogRef.close();
  }

  onClear() {
    this.positionForm.reset();
  }

}
