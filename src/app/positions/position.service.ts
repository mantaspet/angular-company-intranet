import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Position } from './position.model';
import { Subject } from 'rxjs/Subject';
import { apiPath } from '../../environments/environment';
import { MatSnackBar } from '@angular/material';

@Injectable()
export class PositionService {
  positions: Position[];
  positionsChanged = new Subject<Position[]>();
  loading = new Subject<boolean>();

  constructor(
    private httpClient: HttpClient,
    private snackBar: MatSnackBar) { }

  getPositions() {
    this.loading.next(true);
    this.httpClient.get(apiPath + '/positions').subscribe(
      (response: any) => {
        this.positions = response.items.data;
        this.positionsChanged.next(this.positions);
        this.loading.next(false);
      },
      (error: any) => {
        console.log(error);
        this.openSnackBar('Įvyko klaida. Kreipkitės į sistemos administratorių.', '');
      }
    );
  }

  addPosition(position: Position) {
    this.loading.next(true);
    this.httpClient.post(apiPath + '/positions', position).subscribe(
      (response: any) => {
        this.positions.push(response.item);
        this.positionsChanged.next(this.positions);
        this.openSnackBar('Pareigos sukurtos.', '');
      },
      (error: any) => {
        console.log(error);
        this.openSnackBar('Įvyko klaida. Kreipkitės į sistemos administratorių.', '');
      }
    );
  }

  updatePosition(position: Position) {
    this.loading.next(true);
    this.httpClient.put(apiPath + '/positions/' + position.id, position).subscribe(
      () => {
        this.openSnackBar('Pareigos atnaujintos.', '');
      },
      (error: any) => {
        console.log(error);
        this.openSnackBar('Įvyko klaida. Kreipkitės į sistemos administratorių.', '');
      }
    );
  }

  deletePosition(position: Position) {
    this.loading.next(true);
    this.httpClient.delete(apiPath + '/positions/' + position.id).subscribe(
      () => {
        const index = this.positions.indexOf(position);
        if (index >= 0) {
          this.positions.splice(index, 1);
          this.positionsChanged.next(this.positions);
          this.openSnackBar('Pareigos ištrintos.', '');
        }
      },
      (error: any) => {
        console.log(error);
        this.openSnackBar('Įvyko klaida. Kreipkitės į sistemos administratorių.', '');
      }
    );
  }

  openSnackBar(message: string, action: string) {
    this.loading.next(false);
    this.snackBar.open(message, action, {
      duration: 2500,
    });
  }
}
