import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { OfficeService } from '../office.service';
import { MatDialogRef } from '@angular/material';
import { MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-office-edit',
  templateUrl: './office-edit.component.html',
  styleUrls: ['./office-edit.component.css']
})
export class OfficeEditComponent implements OnInit {
  // editMode = false;
  wasChanged = false;
  @ViewChild('f') officeForm: NgForm;

  constructor(
    private officeService: OfficeService,
    public dialogRef: MatDialogRef<OfficeEditComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
    if (this.data.editMode) {
      // this.editMode = this.data.editMode;
      const tmpOffice = {...this.data.office};
      this.dialogRef.beforeClose().subscribe(
        () => {
          if (!this.wasChanged) {
            this.officeForm.setValue({
              name: tmpOffice.name,
              country: tmpOffice.country,
              city: tmpOffice.city,
              address: tmpOffice.address
            });
          }
        }
      );
    }
  }

  onSubmitOffice(form: NgForm, office: any) {
    if (this.data.editMode) {
      this.officeService.updateOffice(office);
      this.wasChanged = true;
    } else {
      this.officeService.addOffice(form.value);
      this.wasChanged = true;
    }
    // this.editMode = false;
    this.dialogRef.close();
  }

  onClear() {
    this.officeForm.reset();
  }

}
