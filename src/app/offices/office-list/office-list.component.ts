import { Component, OnInit } from '@angular/core';
import { DataSource } from '@angular/cdk/collections';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import { Office } from '../office.model';
import { OfficeService } from '../office.service';
import { MatDialog } from '@angular/material';
import { OfficeEditComponent } from '../office-edit/office-edit.component';

let officeObservable;

@Component({
  selector: 'app-office-list',
  styleUrls: ['office-list.component.css'],
  templateUrl: 'office-list.component.html',
})
export class OfficeListComponent implements OnInit {
  displayedColumns = ['name', 'country', 'city', 'address']; // 'delete'
  dataSource = new OfficeDataSource();
  loading = false;

  constructor(
    private officeService: OfficeService,
    public dialog: MatDialog) {}

  ngOnInit() {
    this.officeService.loading.subscribe(
      (status: boolean) => {
        this.loading = status;
      }
    );
    this.officeService.getOffices();
    officeObservable = this.officeService.officesChanged;
  }

  onNewOffice(): void {
    const officeDialog = this.dialog.open(OfficeEditComponent, {
      data: {
        editMode: false,
        office: {
          name: '',
          country: '',
          city: '',
          address: ''
        }
      }
    });
  }

  onUpdateOffice(office: Office) {
    const officeDialog = this.dialog.open(OfficeEditComponent, {
      data: {
        editMode: true,
        office: office
      }
    });
  }

  // onDeleteOffice(office, event) {
  //   event.stopPropagation();
  //   const confirmDialog = this.dialog.open(ConfirmBoxComponent, {
  //     data: {
  //       type: 'ofisą'
  //     }
  //   });
  //   confirmDialog.afterClosed().subscribe(confirmed => {
  //     if (confirmed) {
  //       this.officeService.deleteOffice(office);
  //     }
  //   });
  // }
}

export class OfficeDataSource extends DataSource<any> {
  connect(): Observable<Office[]> {
    return officeObservable;
  }

  disconnect() {}
}
