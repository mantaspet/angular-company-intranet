import { MatSnackBar } from '@angular/material';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Office } from './office.model';
import { Subject } from 'rxjs/Subject';
import { apiPath } from '../../environments/environment';

@Injectable()
export class OfficeService {
  offices: Office[];
  officesChanged = new Subject<Office[]>();
  loading = new Subject<boolean>();

  constructor(
    private httpClient: HttpClient,
    private snackBar: MatSnackBar) { }

  getOffices() {
    this.loading.next(true);
    this.httpClient.get(apiPath + '/offices').subscribe(
      (response: any) => {
        this.offices = response.items.data;
        this.officesChanged.next(this.offices);
        this.loading.next(false);
      },
      (error: any) => {
        console.log(error);
        this.openSnackBar('Įvyko klaida. Kreipkitės į sistemos administratorių.', '');
      }
    );
  }

  addOffice(office: Office) {
    this.loading.next(true);
    this.httpClient.post(apiPath + '/offices', office).subscribe(
      (response: any) => {
        this.offices.push(response.item);
        this.officesChanged.next(this.offices);
        this.openSnackBar('Ofisas sukurtas.', '');
      },
      (error: any) => {
        console.log(error);
        this.openSnackBar('Įvyko klaida. Kreipkitės į sistemos administratorių.', '');
      }
    );
  }

  updateOffice(office: Office) {
    this.loading.next(true);
    this.httpClient.put(apiPath + '/offices/' + office.id, office).subscribe(
      () => {
        this.openSnackBar('Ofisas atnaujintas.', '');
      },
      (error: any) => {
        console.log(error);
        this.openSnackBar('Įvyko klaida. Kreipkitės į sistemos administratorių.', '');
      }
    );
  }

  deleteOffice(office: Office) {
    this.loading.next(true);
    this.httpClient.delete(apiPath + '/offices/' + office.id).subscribe(
      () => {
        const index = this.offices.indexOf(office);
        if (index >= 0) {
          this.offices.splice(index, 1);
          this.officesChanged.next(this.offices);
          this.openSnackBar('Ofisas ištrintas.', '');
        }
      },
      (error: any) => {
        console.log(error);
        this.openSnackBar('Įvyko klaida. Kreipkitės į sistemos administratorių.', '');
      }
    );
  }

  openSnackBar(message: string, action: string) {
    this.loading.next(false);
    this.snackBar.open(message, action, {
      duration: 2500,
    });
  }
}
