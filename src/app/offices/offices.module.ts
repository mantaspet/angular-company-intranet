import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OfficeEditComponent } from './office-edit/office-edit.component';
import { FormsModule } from '@angular/forms';
import { OfficeService } from './office.service';
import { OfficeListComponent } from './office-list/office-list.component';
import {
  MatTableModule, MatToolbarModule, MatButtonModule, MatInputModule, MatFormFieldModule, MatCardModule, MatDialogModule,
  MatSnackBarModule, MatIconModule, MatTooltipModule, MatProgressBarModule
} from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatCardModule,
    MatButtonModule,
    MatTableModule,
    MatToolbarModule,
    MatDialogModule,
    MatSnackBarModule,
    MatIconModule,
    MatTooltipModule,
    MatProgressBarModule
  ],
  declarations: [OfficeEditComponent, OfficeListComponent],
  providers: [OfficeService],
  entryComponents: [OfficeEditComponent]
})
export class OfficesModule { }
