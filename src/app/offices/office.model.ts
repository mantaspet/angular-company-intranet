export class Office {
  id: number;
  name: string;
  country: string;
  city: string;
  address: string;
}
