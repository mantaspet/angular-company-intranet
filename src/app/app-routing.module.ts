import { ChangePasswordComponent } from './authentication/change-password/change-password.component';
import { DocumentListComponent } from './documents/document-list/document-list.component';
import { AssetListComponent } from './assets/asset-list/asset-list.component';
import { HolidayHistoryListComponent } from './employees/holiday-history/holiday-history-list/holiday-history-list.component';
import { EmployeeHistoryListComponent } from './employees/employee-history/employee-history-list/employee-history-list.component';
import { DepartmentListComponent } from './departments/department-list/department-list.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './core/home/home.component';
import { EmployeeListComponent } from './employees/employee-list/employee-list.component';
import { SigninComponent } from './authentication/signin/signin.component';
import { OfficeEditComponent } from './offices/office-edit/office-edit.component';
import { OfficeListComponent } from './offices/office-list/office-list.component';
import { AuthenticationGuardService as AuthGuard } from './authentication/authentication-guard.service';
import { PositionListComponent } from './positions/position-list/position-list.component';
import { SalaryHistoryListComponent } from './employees/salary-history/salary-history-list/salary-history-list.component';
import { HolidayHistoryEditComponent } from './employees/holiday-history/holiday-history-edit/holiday-history-edit.component';
import { AssetAssignmentListComponent } from './assets/asset-assignments/asset-assignment-list/asset-assignment-list.component';
import { AssetAssignmentEditComponent } from './assets/asset-assignments/asset-assignment-edit/asset-assignment-edit.component';
import { AuthorizationGuardService as AuthorizationGuard } from './shared/authorization.guard';

const appRoutes: Routes = [
  { path: 'signin', component: SigninComponent },
  { path: 'change-password', component: ChangePasswordComponent, canActivate: [AuthGuard] },
  { path: '', component: DocumentListComponent, canActivate: [AuthGuard] },
  { path: 'employees', component: EmployeeListComponent, canActivate: [AuthGuard, AuthorizationGuard], data: {
    roles: ['manager', 'administrator']
  } },
  { path: 'employees/:id/salary_history', component: SalaryHistoryListComponent, canActivate: [AuthGuard, AuthorizationGuard], data: {
    first_name: '', last_name: '', page: '', roles: ['manager', 'administrator']
  } },
  { path: 'salary_history', component: SalaryHistoryListComponent, canActivate: [AuthGuard, AuthorizationGuard], data: {
    page: '', roles: ['employee', 'manager']
  } },
  { path: 'employees/:id/employee_history', component: EmployeeHistoryListComponent, canActivate: [AuthGuard, AuthorizationGuard], data: {
    first_name: '', last_name: '', roles: ['manager', 'administrator']
  } },
  { path: 'employee_history', component: EmployeeHistoryListComponent, canActivate: [AuthGuard, AuthorizationGuard], data: {
    page: '', roles: ['employee', 'manager']
  } },
  { path: 'employees/:id/holiday_history', component: HolidayHistoryListComponent, canActivate: [AuthGuard, AuthorizationGuard], data: {
    first_name: '', last_name: '', page: '', roles: ['manager', 'administrator']
  } },
  { path: 'holiday_history', component: HolidayHistoryListComponent, canActivate: [AuthGuard, AuthorizationGuard], data: {
    page: '', roles: ['employee', 'manager']
  } },
  { path: 'employees/holiday_history/:requests', component: HolidayHistoryListComponent, canActivate: [AuthGuard, AuthorizationGuard],
    data: {
      first_name: '', last_name: '', roles: ['manager', 'administrator']
    } },
  { path: 'employees/:my_holiday_requests', component: HolidayHistoryListComponent, canActivate: [AuthGuard, AuthorizationGuard], data: {
    first_name: '', last_name: '', roles: ['employee', 'manager']
  } },
  // { path: 'new_holiday', component: HolidayHistoryEditComponent, canActivate: [AuthGuard] },
  { path: 'offices', component: OfficeListComponent, canActivate: [AuthGuard, AuthorizationGuard], data: { roles: ['administrator'] } },
  { path: 'departments', component: DepartmentListComponent, canActivate: [AuthGuard, AuthorizationGuard],
    data: { roles: ['administrator'] } },
  { path: 'positions', component: PositionListComponent, canActivate: [AuthGuard, AuthorizationGuard], data: { roles: ['administrator'] } },
  { path: 'assets', component: AssetListComponent, canActivate: [AuthGuard, AuthorizationGuard], data: { roles: ['administrator'] } },
  { path: 'asset_assignments', component: AssetAssignmentListComponent, canActivate: [AuthGuard], data: {
    page: ''
  } },
  // { path: 'asset_assignments/:new_request', component: AssetAssignmentEditComponent, canActivate: [AuthGuard] }, // Naujas prasymas
  { path: 'documents', component: DocumentListComponent, canActivate: [AuthGuard] },
  { path: '**', redirectTo: 'signin' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {

}
