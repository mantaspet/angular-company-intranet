import { AssetsModule } from './assets/assets.module';
import { PositionsModule } from './positions/positions.module';
import { SharedModule } from './shared/shared.module';
import { AuthenticationGuardService } from './authentication/authentication-guard.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { AuthenticationModule } from './authentication/authentication.module';
import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { EmployeesModule } from './employees/employees.module';
import { OfficesModule } from './offices/offices.module';
import { DepartmentsModule } from './departments/departments.module';
import { DocumentsModule } from './documents/documents.module';
import { MatProgressBarModule } from '@angular/material';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    SharedModule,
    BrowserModule,
    CoreModule,
    AuthenticationModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    EmployeesModule,
    OfficesModule,
    DepartmentsModule,
    PositionsModule,
    AssetsModule,
    DocumentsModule,
    MatProgressBarModule
  ],
  providers: [
    AuthenticationGuardService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
