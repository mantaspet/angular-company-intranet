import { AuthenticationService } from './../authentication.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {
  hide1 = true;
  hide2 = true;
  loading = false;
  curent_password = '';
  new_password = '';
  repeat_new = '';
  @ViewChild('f') changePasswordForm: NgForm;

  constructor(private authService: AuthenticationService) { }

  ngOnInit() {
    this.authService.loading.subscribe(
      (status: boolean) => {
        this.loading = status;
      }
    );
  }

  onChangePassword(form: NgForm) {
    const current_password = form.value.current;
    const new_password = form.value.new;
    this.authService.changePassword(current_password, new_password);
  }

}
