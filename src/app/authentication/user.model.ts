import { Contact } from './../employees/contact.model';

export class User {
  id: number;
  email: string;
  password: string;
  role: string;
  contact_id: number;
  contact: Contact;
}
