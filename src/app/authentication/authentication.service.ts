import { Subject } from 'rxjs/Subject';
import { HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { authPath, grant_type, client_id, client_secret, apiPath } from '../../environments/environment';
import { JwtHelperService } from '@auth0/angular-jwt';
import { MatSnackBar } from '@angular/material';


@Injectable()
export class AuthenticationService {
  loading = new Subject<boolean>();

  constructor(
    private httpClient: HttpClient,
    private router: Router,
    private snackBar: MatSnackBar,
    public jwtHelperService: JwtHelperService) { }

  signinUser(email: string, password: string, rememberPassword: any) {
    this.loading.next(true);
    this.httpClient.post(authPath, {
      grant_type: grant_type,
      client_id: client_id,
      client_secret: client_secret,
      username: email,
      password: password,
      scope: ''
    }).subscribe(
      (response: any) => {
        console.log(response);
        localStorage.setItem('access_token', response.access_token);
        this.httpClient.get(apiPath + '/get-authenticated-user').subscribe(
          (response2: any) => {
            if (response2.item.is_active) {
              console.log(response2);
              const name = response2.item.contact.first_name + ' ' + response2.item.contact.last_name;
              const role = response2.item.role;
              localStorage.setItem('name', name);
              localStorage.setItem('role', role);
              this.router.navigate(['']);
              this.openSnackBar('Prisijungta sėkmingai.', '');
            } else {
              console.log(response2);
              localStorage.removeItem('access_token');
              this.openSnackBar('Ši paskyra suspenduota. Prašome kreiptis į sistemos administratorių.', '');
            }
          }
        );
      }, (bad_response: any) => {
        console.log(bad_response);
        if (bad_response.error.error === 'invalid_credentials') {
          this.openSnackBar('Neteisingi prisijungimo duomenys.', '');
        }
      }
    );
  }

  signoutUser() {
    localStorage.removeItem('name');
    localStorage.removeItem('role');
    localStorage.removeItem('access_token');
    this.router.navigate(['/signin']);
  }

  isAuthenticated() {
    const token = localStorage.getItem('access_token');
    if (token == null) {
      return false;
    } else {
      return !this.jwtHelperService.isTokenExpired(token);
    }
  }

  changePassword(current: string, new_password: string) {
    this.loading.next(true);
    const requestBody = {
      current_password: current,
      new_password: new_password
    }
    console.log(requestBody);
    this.httpClient.put(apiPath + '/change-password', requestBody).subscribe(
      (response: any) => {
        console.log(response);
        this.openSnackBar('Slaptažodis pakeistas.', '');
      }, (error: any) => {
        this.openSnackBar(error.error.item, '');
      }
    );
  }

  openSnackBar(message: string, action: string) {
    this.loading.next(false);
    this.snackBar.open(message, action, {
      duration: 2500,
    });
  }

}
