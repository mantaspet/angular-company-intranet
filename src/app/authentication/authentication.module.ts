import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { JwtModule } from '@auth0/angular-jwt';

import { AuthenticationService } from './authentication.service';
import { SigninComponent } from './signin/signin.component';
import { FormsModule } from '@angular/forms';
import { MatCheckboxModule, MatInputModule, MatButtonModule, MatCardModule, MatSnackBarModule, MatIconModule,
  MatProgressBarModule } from '@angular/material';
import { whitelistedDomains } from '../../environments/environment';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [
    SigninComponent,
    ChangePasswordComponent
  ],
  imports: [
    HttpClientModule,
    CommonModule,
    FormsModule,
    MatCheckboxModule,
    MatInputModule,
    MatButtonModule,
    MatCardModule,
    MatSnackBarModule,
    MatIconModule,
    MatProgressBarModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: () => {
          return localStorage.getItem('access_token');
        },
        whitelistedDomains: whitelistedDomains
      }
    })
  ],
  providers: [
    AuthenticationService
  ]
})
export class AuthenticationModule { }
