import { MatSnackBar } from '@angular/material';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Asset } from './asset.model';
import { Subject } from 'rxjs/Subject';
import { apiPath } from '../../environments/environment';
import { DatePipe } from '@angular/common';

@Injectable()
export class AssetService {
  assets: Asset[];
  totalValue: number;
  assetsChanged = new Subject<Asset[]>();
  totalValueChanged = new Subject<number>();
  loading = new Subject<boolean>();

  constructor(
    private httpClient: HttpClient,
    private snackBar: MatSnackBar,
    private datePipe: DatePipe) { }

  getAssets() {
    this.loading.next(true);
    this.httpClient.get(apiPath + '/assets').subscribe(
      (response: any) => {
        console.log(response);
        this.assets = response.items.data;
        this.getTotalValue();
        this.assetsChanged.next(this.assets);
        this.loading.next(false);
      },
      (error: any) => {
        console.log(error);
        this.openSnackBar('Įvyko klaida. Kreipkitės į sistemos administratorių.', '');
      }
    );
  }

  getUnassignedAssets() {
    this.loading.next(true);
    this.httpClient.get(apiPath + '/assets/unassigned').subscribe(
      (response: any) => {
        this.assets = response.items.data;
        this.assetsChanged.next(this.assets);
        this.loading.next(false);
      },
      (error: any) => {
        console.log(error);
        this.openSnackBar('Įvyko klaida. Kreipkitės į sistemos administratorių.', '');
      }
    );
  }

  getExpenses(startDate: Date, endDate: Date) {
    this.loading.next(true);
    const requestBody = {
      start: startDate ? startDate : undefined,
      end: endDate ? endDate : undefined
    }
    this.httpClient.post(apiPath + '/expenses', requestBody).subscribe(
      (response: any) => {
        console.log(response);
        this.assets = response.items;
        this.getTotalValue();
        this.assetsChanged.next(this.assets);
        this.loading.next(false);
      },
      (error: any) => {
        console.log(error);
        this.openSnackBar('Įvyko klaida. Kreipkitės į sistemos administratorių.', '');
      }
    );
  }

  getTotalValue() {
    this.totalValue = 0;
    this.assets.forEach(element => {
      this.totalValue = this.totalValue + (+element.value);
    });
    this.totalValueChanged.next(this.totalValue);
  }

  addAsset(asset: Asset) {
    this.loading.next(true);
    this.httpClient.post(apiPath + '/assets', asset).subscribe(
      (response: any) => {
        response.item.is_assigned = false;
        if (!response.item.value) {
          response.item.value = '';
        }
        if (response.item.acquisition_date.date) {
          response.item.acquisition_date = this.datePipe.transform(response.item.acquisition_date.date, 'yyyy-MM-dd');
        }
        this.assets.push(response.item);
        this.totalValue = this.totalValue + (+response.item.value);
        this.totalValueChanged.next(this.totalValue);
        this.assetsChanged.next(this.assets);
        this.openSnackBar('Turto įrašas sukurtas.', '');
      },
      (error: any) => {
        console.log(error);
        this.openSnackBar('Įvyko klaida. Kreipkitės į sistemos administratorių.', '');
      }
    );
  }

  updateAsset(asset: Asset) {
    console.log(asset);
    this.loading.next(true);
    this.httpClient.put(apiPath + '/assets/' + asset.id, asset).subscribe(
      (response: any) => {
        this.totalValue = this.totalValue - (+response.item.oldValue);
        this.totalValue = this.totalValue + (+response.item.value);
        this.totalValueChanged.next(this.totalValue);
        this.openSnackBar('Turto įrašas atnaujintas.', '');
      },
      (error: any) => {
        console.log(error);
        this.openSnackBar('Įvyko klaida. Kreipkitės į sistemos administratorių.', '');
      }
    );
  }

  openSnackBar(message: string, action: string) {
    this.loading.next(false);
    this.snackBar.open(message, action, {
      duration: 2500,
    });
  }
}
