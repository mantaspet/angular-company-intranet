import { AssetAssignmentListComponent } from './asset-assignments/asset-assignment-list/asset-assignment-list.component';
import { AssetAssignmentEditComponent } from './asset-assignments/asset-assignment-edit/asset-assignment-edit.component';
import { AssetAssignmentService } from './asset-assignments/asset-assignment.service';

import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { AssetEditComponent } from './asset-edit/asset-edit.component';
import { FormsModule } from '@angular/forms';
import { AssetService } from './asset.service';
import { AssetListComponent } from './asset-list/asset-list.component';
import {
  MatTableModule, MatToolbarModule, MatButtonModule, MatInputModule, MatFormFieldModule, MatCardModule, MatDialogModule,
  MatSnackBarModule, MatIconModule, MatDatepickerModule, MatCheckboxModule, MatSelectModule, MatAutocompleteModule, MatTooltipModule,
  MatProgressBarModule
} from '@angular/material';
import { AssetReturnComponent } from './asset-assignments/asset-return/asset-return.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatCardModule,
    MatButtonModule,
    MatTableModule,
    MatToolbarModule,
    MatDialogModule,
    MatSnackBarModule,
    MatIconModule,
    MatDatepickerModule,
    MatCheckboxModule,
    MatSelectModule,
    MatAutocompleteModule,
    MatTooltipModule,
    MatProgressBarModule
  ],
  declarations: [
    AssetEditComponent,
    AssetListComponent,
    AssetAssignmentListComponent,
    AssetAssignmentEditComponent,
    AssetReturnComponent,
  ],
  providers: [AssetService, AssetAssignmentService, DatePipe],
  entryComponents: [AssetEditComponent, AssetAssignmentEditComponent, AssetReturnComponent]
})
export class AssetsModule { }
