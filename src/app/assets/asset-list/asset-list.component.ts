import { Component, OnInit } from '@angular/core';
import { DataSource } from '@angular/cdk/collections';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import { Asset } from '../asset.model';
import { AssetService } from '../asset.service';
import { MatDialog } from '@angular/material';
import { AssetEditComponent } from '../asset-edit/asset-edit.component';

let assetObservable;

@Component({
  selector: 'app-asset-list',
  styleUrls: ['asset-list.component.css'],
  templateUrl: 'asset-list.component.html',
})
export class AssetListComponent implements OnInit {
  startDate: Date;
  endDate: Date;
  totalValue: number;
  loading = false;
  displayedColumns = ['name', 'value', 'acquisition_date', 'type', 'is_assigned'];
  dataSource = new AssetDataSource();

  constructor(
    private assetService: AssetService,
    public dialog: MatDialog) {}

  ngOnInit() {
    this.assetService.loading.subscribe(
      (status: boolean) => {
        this.loading = status;
      }
    );
    assetObservable = this.assetService.assetsChanged;
    this.assetService.totalValueChanged.subscribe(
      (value: number) => {
        this.totalValue = value;
      }
    );
    this.assetService.getAssets();
  }

  onNewAsset(): void {
    const assetDialog = this.dialog.open(AssetEditComponent, {
      data: {
        editMode: false,
        asset: {
          name: '',
          value: '',
          acquisition_date: '',
          type: '',
          is_assigned: ''
        }
      }
    });
  }

  onUpdateAsset(asset: Asset) {
    const assetDialog = this.dialog.open(AssetEditComponent, {
      data: {
        editMode: true,
        asset: asset
      }
    });
  }

  onShowExpenses() {
    this.assetService.getExpenses(this.startDate, this.endDate);
  }
}

export class AssetDataSource extends DataSource<any> {
  connect(): Observable<Asset[]> {
    return assetObservable;
  }

  disconnect() {}
}
