import { DatePipe } from '@angular/common';
import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AssetService } from '../asset.service';
import { MatDialogRef } from '@angular/material';
import { MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-asset-edit',
  templateUrl: './asset-edit.component.html',
  styleUrls: ['./asset-edit.component.css']
})
export class AssetEditComponent implements OnInit {
  editMode = false;
  wasChanged = false;
  employee_id: number;
  @ViewChild('f') assetForm: NgForm;

  constructor(
    private assetService: AssetService,
    private datePipe: DatePipe,
    public dialogRef: MatDialogRef<AssetEditComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
    this.employee_id = this.data.employee_id;
    if (this.data.editMode) {
      this.editMode = this.data.editMode;
      const tmpAsset = {...this.data.asset};
      this.dialogRef.beforeClose().subscribe(
        () => {
          if (!this.wasChanged) {
            this.assetForm.setValue({
              name: tmpAsset.name,
              value: tmpAsset.value,
              acquisition_date: tmpAsset.acquisition_date,
              type: tmpAsset.type,
              is_assigned: tmpAsset.is_assigned
            });
          }
        }
      );
    }
  }

  onSubmitAsset(asset: any) {
    if (this.editMode) {
      this.alterDataStructure(asset);
      this.assetService.updateAsset(asset);
      this.wasChanged = true;
    } else {
      this.alterDataStructure(asset);
      this.assetService.addAsset(asset);
      this.wasChanged = true;
    }
    this.editMode = false;
    this.dialogRef.close();
  }

  alterDataStructure(asset: any) {
    this.datePipe.transform(asset.acquisition_date, 'yyyy-MM-dd');
    if (asset.acquisition_date && !this.isValidDate(asset.acquisition_date.toString())) {
      const day = asset.acquisition_date.getDate();
      const month = asset.acquisition_date.getMonth() + 1;
      const year = asset.acquisition_date.getFullYear();
      asset.acquisition_date = year + '-' + month + '-' + day;
    }
    // asset.value = asset.value.toFixed(2);
    return asset;
  }

  isValidDate(dateString: string) {
    const regEx = /^\d{4}-\d{2}-\d{2}$/;
    if (!dateString.match(regEx)) {
      return false;  // Invalid format
    }
    const d = new Date(dateString);
    if (!d.getTime()) {
      return false; // Invalid date (or this could be epoch)
    }
    return d.toISOString().slice(0, 10) === dateString;
  }

  onClear() {
    this.assetForm.setValue({
      name: '',
      value: '',
      acquisition_date: '',
      type: '',
      is_assigned: this.data.asset.is_assigned
    });
  }

}
