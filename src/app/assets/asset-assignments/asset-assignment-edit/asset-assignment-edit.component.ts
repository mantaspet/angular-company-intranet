import { Office } from './../../../offices/office.model';
import { AssetService } from './../../asset.service';
import { Asset } from './../../asset.model';
import { EmployeeService } from './../../../employees/employee.service';
import { Employee } from './../../../employees/employee.model';

import { Subscription } from 'rxjs/Subscription';
import { DatePipe } from '@angular/common';
import { Component, Inject, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AssetAssignmentService } from '../asset-assignment.service';
import { MatDialogRef } from '@angular/material';
import { MAT_DIALOG_DATA } from '@angular/material';
import { OfficeService } from '../../../offices/office.service';

@Component({
  selector: 'app-asset-assignment-edit',
  templateUrl: './asset-assignment-edit.component.html',
  styleUrls: ['./asset-assignment-edit.component.css']
})
export class AssetAssignmentEditComponent implements OnInit, OnDestroy {
  wasChanged = false;
  employees: Employee[];
  offices: Office[];
  assets: Asset[];
  filteredEmployees: Employee[];
  filteredOffices: Office[];
  filteredAssets: Asset[];
  employeesSubscription: Subscription;
  officesSubscription: Subscription;
  assetsSubscription: Subscription;
  @ViewChild('f') assetAssignmentForm: NgForm;

  constructor(
    private assetAssignmentService: AssetAssignmentService,
    private datePipe: DatePipe,
    private employeeService: EmployeeService,
    private officeService: OfficeService,
    private assetService: AssetService,
    public dialogRef: MatDialogRef<AssetAssignmentEditComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
    console.log(this.data.asset_assignment);
    if (this.data.page === 'manage_requests' || !this.data.page) {
      if (this.data.page !== 'manage_requests' && !this.data.editMode) {
        this.getEmployeesList();
        this.getOfficesList();
      }
      this.getAssetsList();
      if (this.data.editMode) {
        const tmpAssetAssignment = JSON.parse(JSON.stringify(this.data));
        console.log(tmpAssetAssignment);
        this.dialogRef.beforeClose().subscribe(
          () => {
            if (!this.wasChanged) {
              if (!this.data.page) {
                this.assetAssignmentForm.setValue({
                  type: tmpAssetAssignment.type,
                  assigned_to: tmpAssetAssignment.type === 'office' ?
                    tmpAssetAssignment.asset_assignment.office :
                    tmpAssetAssignment.asset_assignment.employee,
                  asset: tmpAssetAssignment.asset_assignment.asset,
                  deadline: tmpAssetAssignment.asset_assignment.deadline,
                  assignment_date: tmpAssetAssignment.asset_assignment.assignment_date,
                });
              } else {
                this.assetAssignmentForm.setValue({
                  type: tmpAssetAssignment.type,
                  assigned_to: tmpAssetAssignment.type === 'office' ?
                    tmpAssetAssignment.asset_assignment.office :
                    tmpAssetAssignment.asset_assignment.employee,
                  asset: tmpAssetAssignment.asset_assignment.asset,
                  deadline: tmpAssetAssignment.asset_assignment.deadline,
                  assignment_date: tmpAssetAssignment.asset_assignment.assignment_date,
                  request_message: tmpAssetAssignment.asset_assignment.request_message
                });
              }
            }
          }
        );
      }
    }
  }

  ngOnDestroy() {
    if (this.data.page === 'manage_requests' || !this.data.page) {
      if (this.data.page !== 'manage_requests' && !this.data.editMode) {
        this.employeesSubscription.unsubscribe();
        this.officesSubscription.unsubscribe();
      }
      this.assetsSubscription.unsubscribe();
    }
  }

  onSubmitAssetAssignment(form: NgForm, asset_assignment: any) {
    if (this.data.editMode) {
      this.data.asset_assignment.deadline = this.datePipe.transform(this.data.asset_assignment.deadline, 'yyyy-MM-dd');
      this.data.asset_assignment.assignment_date = this.datePipe.transform(this.data.asset_assignment.assignment_date, 'yyyy-MM-dd');
      this.data.asset_assignment.type = this.data.type;
      this.assetAssignmentService.updateAssetAssignment(asset_assignment);
      this.wasChanged = true;
    } else {
      this.data.asset_assignment.deadline = this.datePipe.transform(this.data.asset_assignment.deadline, 'yyyy-MM-dd');
      this.data.asset_assignment.assignment_date = this.datePipe.transform(this.data.asset_assignment.assignment_date, 'yyyy-MM-dd');
      this.assetAssignmentService.addAssetAssignment(asset_assignment);
      this.wasChanged = true;
    }
    this.dialogRef.close();
  }

  onSubmitAssetRequest() {
    if (this.data.editMode) {
      this.data.asset_assignment.deadline = this.datePipe.transform(this.data.asset_assignment.deadline, 'yyyy-MM-dd');
      this.data.asset_assignment.type = this.data.type;
      this.assetAssignmentService.updateAssetRequest(this.data.asset_assignment);
    } else {
      this.data.asset_assignment.deadline = this.datePipe.transform(this.data.asset_assignment.deadline, 'yyyy-MM-dd');
      this.data.asset_assignment.type = this.data.type;
      this.assetAssignmentService.addAssetRequest(this.data.asset_assignment);
    }
    this.dialogRef.close();
  }

  onConfirmRequest(status: boolean) {
    this.data.asset_assignment.is_confirmed = status;
    if (status) {
      this.data.asset_assignment.deadline = this.datePipe.transform(this.data.asset_assignment.deadline, 'yyyy-MM-dd');
      this.data.asset_assignment.assignment_date = this.datePipe.transform(this.data.asset_assignment.assignment_date, 'yyyy-MM-dd');
      this.data.asset_assignment.asset_id = this.data.asset_assignment.asset.id;
    }
    this.assetAssignmentService.onConfirmRequest(this.data.asset_assignment);
    this.dialogRef.close();
  }


  // ------------------------------------------------------------------------------------

  getEmployeesList() {
    this.employeesSubscription = this.employeeService.employeesChanged.subscribe(
      (response: any) => {
        this.employees = response;
        this.filteredEmployees = this.employees;
      }
    );
    this.employeeService.getEmployees();
  }

  getOfficesList() {
    this.officesSubscription = this.officeService.officesChanged.subscribe(
      (response: any) => {
        this.offices = response;
        this.filteredOffices = this.offices;
      }
    );
    this.officeService.getOffices();
  }

  getAssetsList() {
    this.assetsSubscription = this.assetService.assetsChanged.subscribe(
      (response: any) => {
        this.assets = response;
        this.filteredAssets = this.assets;
      }
    );
    this.assetService.getUnassignedAssets();
  }

  filterEmployees(name: string) {
    this.filteredEmployees = this.employees.filter((employee: Employee) =>
      employee.contact.last_name.toLowerCase().indexOf(name.toLowerCase()) === 0);
  }

  filterOffices(name: string) {
    this.filteredOffices = this.offices.filter(office =>
      office.name.toLowerCase().indexOf(name.toLowerCase()) === 0);
  }

  filterAssets(name: string) {
    this.filteredAssets = this.assets.filter(asset =>
      asset.name.toLowerCase().indexOf(name.toLowerCase()) === 0);
  }

  employeeDisplayFn(employee: Employee): string {
    if (employee !== null) {
      return employee.contact.first_name !== '' ? employee.contact.first_name + ' ' + employee.contact.last_name : '';
    }
    return '';
  }

  officeDisplayFn(office: Office): string {
    if (office !== null) {
      return office.name !== '' ? office.name : '';
    }
    return '';
  }

  assetDisplayFn(asset: Asset): string {
    if (asset !== null) {
      return asset.name !== '' ? asset.name : '';
    }
    return '';
  }

}
