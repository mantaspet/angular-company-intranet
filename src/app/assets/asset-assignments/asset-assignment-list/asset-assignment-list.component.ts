import { AssetReturnComponent } from './../asset-return/asset-return.component';
import { AssetAssignment } from './../asset-assignment-model';
import { Component, OnInit } from '@angular/core';
import { DataSource } from '@angular/cdk/collections';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import { AssetAssignmentService } from '../asset-assignment.service';
import { MatDialog } from '@angular/material';
import { AssetAssignmentEditComponent } from '../asset-assignment-edit/asset-assignment-edit.component';
import { ActivatedRoute, Params } from '@angular/router';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { Subscription } from 'rxjs/Subscription';

let assetAssignmentObservable;

@Component({
  selector: 'app-asset-assignment-list',
  styleUrls: ['asset-assignment-list.component.css'],
  templateUrl: 'asset-assignment-list.component.html',
})
export class AssetAssignmentListComponent implements OnInit, OnDestroy {
  page: string;
  loading = false;
  progressBarSub: Subscription;
  currentDate: any;
  displayedColumns = ['asset', 'assigned_to', 'assignment_date', 'deadline', 'request_message', 'is_confirmed', 'confirmed_by', 'return'];
  dataSource = new AssetAssignmentDataSource();

  constructor(
    private assetAssignmentService: AssetAssignmentService,
    private route: ActivatedRoute,
    public dialog: MatDialog) {}

  ngOnInit() {
    this.progressBarSub = this.assetAssignmentService.loading.subscribe(
      (status: boolean) => {
        this.loading = status;
      }
    );
    this.route.params.subscribe(
      (params: Params) => {
        assetAssignmentObservable = this.assetAssignmentService.assetAssignmentsChanged;
        this.currentDate = Date.now();
        this.page = this.route.snapshot.paramMap.get('page');

        if (this.page === 'manage_requests') {
          this.displayedColumns = ['assigned_to', 'deadline', 'request_message'];
          this.assetAssignmentService.getAssetRequests();
        } else if (this.page === 'my_requests') {
          this.displayedColumns = ['assigned_to', 'deadline', 'request_message', 'is_confirmed', 'confirmed_by'];
          this.assetAssignmentService.getMyRequests();
        } else if (this.page === 'my_assets') {
          this.displayedColumns = ['asset', 'assignment_date', 'deadline', 'return'];
          this.assetAssignmentService.getMyAssets();
        } else {
          this.displayedColumns = ['asset', 'assigned_to', 'assignment_date', 'deadline', 'return'];
          this.assetAssignmentService.getAssetAssignments();
        }
      }
    );
  }

  ngOnDestroy() {
    this.dataSource.disconnect();
    this.progressBarSub.unsubscribe();
  }

  onNewAssetAssignment(): void {
    const assetAssignmentDialog = this.dialog.open(AssetAssignmentEditComponent, {
      data: {
        editMode: false,
        page: this.page,
        asset_assignment: {
          is_confirmed: null,
          deadline: '',
          assignment_date: '',
          return_date: '',
          return_reason: '',
          asset: null,
          office: {
            name: ''
          },
          employee: {
            contact: {
              first_name: '',
              last_name: ''
            }
          }
        }
      }
    });
  }

  onUpdateAssetAssignment(asset_assignment: AssetAssignment) {
    const assetAssignmentDialog = this.dialog.open(AssetAssignmentEditComponent, {
      data: {
        editMode: true,
        page: this.page,
        asset_assignment: asset_assignment,
        type: asset_assignment.office_id ? 'office' : 'employee'
      }
    });
  }

  onReturnAsset(event, asset_assignment: AssetAssignment) {
    event.stopPropagation();
    const assetAssignmentDialog = this.dialog.open(AssetReturnComponent, {
      data: {
        asset_assignment: asset_assignment
      }
    });
  }
}

export class AssetAssignmentDataSource extends DataSource<any> {
  connect(): Observable<AssetAssignment[]> {
    return assetAssignmentObservable;
  }

  disconnect() {}
}
