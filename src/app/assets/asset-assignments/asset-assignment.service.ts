import { AssetAssignment } from './asset-assignment-model';
import { MatSnackBar } from '@angular/material';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs/Subject';
import { apiPath } from '../../../environments/environment';
import { Router } from '@angular/router';

@Injectable()
export class AssetAssignmentService {
  asset_assignments: AssetAssignment[];
  assetAssignmentsChanged = new Subject<AssetAssignment[]>();
  loading = new Subject<boolean>();

  constructor(
    private httpClient: HttpClient,
    private snackBar: MatSnackBar,
    private router: Router
  ) { }

  getAssetAssignments() {
    this.loading.next(true);
    this.httpClient.get(apiPath + '/asset-assignments').subscribe(
      (response: any) => {
        this.loading.next(false);
        this.asset_assignments = response.items.data;
        console.log(response);
        this.assetAssignmentsChanged.next(this.asset_assignments);
      },
      () => {
        this.openSnackBar('Įvyko klaida. Patikrinkite konsolės langą.', '');
      }
    );
  }

  getAssetRequests() {
    this.loading.next(true);
    this.httpClient.get(apiPath + '/asset-assignments/requests').subscribe(
      (response: any) => {
        this.loading.next(false);
        this.asset_assignments = response.items.data;
        console.log(response);
        this.assetAssignmentsChanged.next(this.asset_assignments);
      },
      () => {
        this.openSnackBar('Įvyko klaida. Patikrinkite konsolės langą.', '');
      }
    );
  }

  getMyRequests() {
    this.loading.next(true);
    this.httpClient.get(apiPath + '/asset-assignments/my-requests').subscribe(
      (response: any) => {
        this.loading.next(false);
        this.asset_assignments = response.items.data;
        console.log(response);
        this.assetAssignmentsChanged.next(this.asset_assignments);
      },
      () => {
        this.openSnackBar('Įvyko klaida. Patikrinkite konsolės langą.', '');
      }
    );
  }

  getMyAssets() {
    this.loading.next(true);
    this.httpClient.get(apiPath + '/asset-assignments/my-assets').subscribe(
      (response: any) => {
        this.loading.next(false);
        this.asset_assignments = response.items.data;
        console.log(response);
        this.assetAssignmentsChanged.next(this.asset_assignments);
      },
      () => {
        this.openSnackBar('Įvyko klaida. Patikrinkite konsolės langą.', '');
      }
    );
  }

  addAssetRequest(request: any) {
    console.log(request);
    this.loading.next(true);
    this.router.navigate(['asset_assignments', { page: 'my_requests' }]);
    this.httpClient.post(apiPath + '/asset-assignments/new-request', request).subscribe(
      (response: any) => {
        if (this.asset_assignments) {
          console.log(response);
          response.item.is_confirmed = null;
          this.asset_assignments.push(response.item);
          this.assetAssignmentsChanged.next(this.asset_assignments);
        }
        this.openSnackBar('Turto prašymas pateiktas.', '');
      },
      () => {
        this.openSnackBar('Įvyko klaida. Patikrinkite konsolės langą.', '');
      }
    );
  }

  updateAssetRequest(request: any) {
    this.loading.next(true);
    if (request.type === 'employee' && request.office_id) {
      delete(request.office_id);
    }
    const index = this.asset_assignments.indexOf(request);
    console.log(request);
    this.httpClient.put(apiPath + '/asset-assignments/update-request', request).subscribe(
      (response: any) => {
        if (index >= 0) {
          this.asset_assignments[index] = response.item;
          this.assetAssignmentsChanged.next(this.asset_assignments);
          this.openSnackBar('Turto prašymas atnaujintas.', '');
        }
        console.log(response);
      },
      () => {
        this.openSnackBar('Įvyko klaida. Patikrinkite konsolės langą.', '');
      }
    );
  }

  addAssetAssignment(asset_assignment: any) {
    this.loading.next(true);
    const requestBody = {
      deadline: asset_assignment.deadline,
      assignment_date: asset_assignment.assignment_date,
      employee_id: asset_assignment.employee.id,
      asset_id: asset_assignment.asset.id,
      is_confirmed: true,
      office_id: null
    };
    if (asset_assignment.office) {
      requestBody.office_id = asset_assignment.office.id;
    }
    console.log(requestBody);
    this.httpClient.post(apiPath + '/asset-assignments', requestBody).subscribe(
      (response: any) => {
        if (this.asset_assignments) {
          this.asset_assignments.push(response.item);
          console.log(response);
          this.assetAssignmentsChanged.next(this.asset_assignments);
        }
        this.openSnackBar('Turto prašymas pateiktas.', '');
      },
      () => {
        this.openSnackBar('Įvyko klaida. Patikrinkite konsolės langą.', '');
      }
    );
  }

  onConfirmRequest(asset_assignment: AssetAssignment) {
    console.log(asset_assignment);
    this.loading.next(true);
    let requestBody;
    if (asset_assignment.is_confirmed) {
      requestBody = {
        deadline: asset_assignment.deadline,
        assignment_date: asset_assignment.assignment_date,
        asset_id: asset_assignment.asset_id,
        is_confirmed: asset_assignment.is_confirmed
      }
    } else {
      requestBody = {
        is_confirmed: asset_assignment.is_confirmed
      }
    }
    const index = this.asset_assignments.indexOf(asset_assignment);
    this.httpClient.put(apiPath + '/asset-assignments/' + asset_assignment.id, requestBody).subscribe(
      (response: any) => {
        if (index >= 0) {
          this.asset_assignments.splice(index, 1);
          this.assetAssignmentsChanged.next(this.asset_assignments);
          this.openSnackBar('Prašymas patvirtintas.', '');
        }
        console.log(response);
      },
      () => {
        this.openSnackBar('Įvyko klaida. Patikrinkite konsolės langą.', '');
      }
    );
  }

  updateAssetAssignment(asset_assignment: AssetAssignment) {
    this.loading.next(true);
    // asset_assignments.responsible_employee_id = asset_assignments.responsible_employee.id;
    this.httpClient.put(apiPath + '/asset-assignments/' + asset_assignment.id, asset_assignment).subscribe(
      () => {
        this.openSnackBar('Priskirto turto įrašas atnaujintas.', '');
      },
      () => {
        this.openSnackBar('Įvyko klaida. Patikrinkite konsolės langą.', '');
      }
    );
  }

  returnAsset(asset_assignment: AssetAssignment) {
    this.loading.next(true);
    const requestBody = {
      id: asset_assignment.id,
      return_reason: asset_assignment.return_reason
    }
    const index = this.asset_assignments.indexOf(asset_assignment);
    this.httpClient.put(apiPath + '/asset-assignments/return-asset', requestBody).subscribe(
      () => {
        if (index >= 0) {
          this.asset_assignments.splice(index, 1);
          this.assetAssignmentsChanged.next(this.asset_assignments);
          this.openSnackBar('Turtas grąžintas.', '');
        }
      },
      () => {
        this.openSnackBar('Įvyko klaida. Patikrinkite konsolės langą.', '');
      }
    );
  }

  openSnackBar(message: string, action: string) {
    this.loading.next(false);
    this.snackBar.open(message, action, {
      duration: 2500,
    });
  }
}
