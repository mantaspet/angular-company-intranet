import { AssetAssignmentService } from './../asset-assignment.service';
import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-asset-return',
  templateUrl: './asset-return.component.html',
  styleUrls: ['./asset-return.component.css']
})
export class AssetReturnComponent {

  constructor(
    private assetAssignmentService: AssetAssignmentService,
    public dialogRef: MatDialogRef<AssetReturnComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  onConfirm() {
    this.assetAssignmentService.returnAsset(this.data.asset_assignment);
    this.dialogRef.close();
  }

  onCancel() {
    this.dialogRef.close();
  }
}
