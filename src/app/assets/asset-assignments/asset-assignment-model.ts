import { Employee } from './../../employees/employee.model';
import { Asset } from './../asset.model';
import { User } from '../../authentication/user.model';
import { Office } from '../../offices/office.model';

export class AssetAssignment {
    id: number;
    request_message: string;
    is_confirmed: boolean;
    deadline: Date;
    assignment_date: Date;
    return_date: Date;
    return_reason: null;
    asset_id: number;
    office_id: number;
    employee_id: number;
    confirmed_by: number;

    asset: Asset;
    employee: Employee;
    office: Office;
    confirmed_by_user: User;
}
