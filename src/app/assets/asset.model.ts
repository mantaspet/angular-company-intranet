export class Asset {
    id: number;
    name: string;
    value: number;
    acquisition_date: Date;
    is_assigned: boolean;
    type: string;
}