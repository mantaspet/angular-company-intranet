import { DatePipe } from '@angular/common';
import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { DocumentService } from '../document.service';
import { MatDialogRef } from '@angular/material';
import { MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-nti-application-request-edit',
  templateUrl: './nti-application-request-edit.component.html',
  styleUrls: ['./nti-application-request-edit.component.css']
})
export class NtiApplicationRequestEditComponent implements OnInit {
  wasChanged = false;
  @ViewChild('f') documentForm: NgForm;

  constructor(
    private documentService: DocumentService,
    private datePipe: DatePipe,
    public dialogRef: MatDialogRef<NtiApplicationRequestEditComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
    if (this.data.editMode) {
      const tmpDocument = JSON.parse(JSON.stringify(this.data.document));
      console.log(tmpDocument);
      this.dialogRef.beforeClose().subscribe(
        () => {
          if (!this.wasChanged) {
            console.log(tmpDocument);
            this.documentForm.setValue({
              title: tmpDocument.title,
              company_name: tmpDocument.nti_application_request.company_name,
              first_name: tmpDocument.nti_application_request.first_name,
              last_name: tmpDocument.nti_application_request.last_name,
              nin: tmpDocument.nti_application_request.nin,
              country: tmpDocument.nti_application_request.country,
              city: tmpDocument.nti_application_request.city,
              address: tmpDocument.nti_application_request.address,
              request_date: tmpDocument.nti_application_request.request_date,
              nti_application_date: tmpDocument.nti_application_request.nti_application_date,
              children_info: tmpDocument.nti_application_request.children_info ? tmpDocument.nti_application_request.children_info : ''
            });
          }
        }
      );
    }
  }

  onSubmitDocument() {
    if (!this.data.editMode) {
      this.transformDates();
      this.documentService.addNtiApplicationRequest(this.data.document);
      this.wasChanged = true;
    } else {
      this.transformDates();
      this.documentService.updateNtiApplicationRequest(this.data.document);
      this.wasChanged = true;
    }
    this.dialogRef.close();
  }

  transformDates() {
    this.data.document.nti_application_request.request_date =
      this.datePipe.transform(this.data.document.nti_application_request.request_date, 'yyyy-MM-dd');
    this.data.document.nti_application_request.nti_application_date =
      this.datePipe.transform(this.data.document.nti_application_request.nti_application_date, 'yyyy-MM-dd');
  }

  onClear() {
    this.documentForm.reset();
  }

}
