export class NtiApplicationRequest {
    id: number;
    document_id: number;
    company_name: string;
    first_name: string;
    last_name: string;
    nin: string;
    country: string;
    city: string;
    address: string;
    request_date: Date;
    nti_application_date: Date;
    children_info: string;
}