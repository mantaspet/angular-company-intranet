import { SalaryTransactionRequestEditComponent } from './../salary-transaction-request-edit/salary-transaction-request-edit.component';
import { Component, OnInit } from '@angular/core';
import { DataSource } from '@angular/cdk/collections';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import { Document } from '../document.model';
import { DocumentService } from '../document.service';
import { MatDialog } from '@angular/material';
import { EmploymentContractEditComponent } from '../employment-contract-edit/employment-contract-edit.component';
import { NtiApplicationRequestEditComponent } from '../nti-application-request-edit/nti-application-request-edit.component';
import { ConfirmBoxComponent } from '../../shared/confirm-box/confirm-box.component';

let documentObservable;

@Component({
  selector: 'app-document-list',
  styleUrls: ['document-list.component.css'],
  templateUrl: 'document-list.component.html',
})
export class DocumentListComponent implements OnInit {
  displayedColumns = ['title', 'type', 'createPDF', 'delete']; // 'delete'
  dataSource = new DocumentDataSource();
  loading = false;

  constructor(
    private documentService: DocumentService,
    public dialog: MatDialog) {}

  ngOnInit() {
    this.documentService.loading.subscribe(
      (status: boolean) => {
        this.loading = status;
      }
    );
    this.documentService.getDocuments();
    documentObservable = this.documentService.documentsChanged;
  }

  onUpdateDocument(document: Document) {
    if (document.related_document_type === 'employment_contract') {
      const documentDialog = this.dialog.open(EmploymentContractEditComponent, {
        data: {
          editMode: true,
          document: document
        }
      });
    } else if (document.related_document_type === 'nti_application_request') {
      const documentDialog = this.dialog.open(NtiApplicationRequestEditComponent, {
        data: {
          editMode: true,
          document: document
        }
      });
    } else if (document.related_document_type === 'salary_transaction_request') {
      const documentDialog = this.dialog.open(SalaryTransactionRequestEditComponent, {
        data: {
          editMode: true,
          document: document
        }
      });
    }
  }

  onNewEmploymentContract(): void {
    const documentDialog = this.dialog.open(EmploymentContractEditComponent, {
      data: {
        editMode: false,
        document: {
          employment_contract: {}
        }
      }
    });
  }

  onNewNtiApplicationRequest(): void {
    const documentDialog = this.dialog.open(NtiApplicationRequestEditComponent, {
      data: {
        editMode: false,
        document: {
          nti_application_request: {}
        }
      }
    });
  }

  onNewSalaryTransactionRequest(): void {
    const documentDialog = this.dialog.open(SalaryTransactionRequestEditComponent, {
      data: {
        editMode: false,
        document: {
          salary_transaction_request: {}
        }
      }
    });
  }

  onCreatePDF(document, event) {
    event.stopPropagation();
    this.documentService.createPDF(document.id, document.url, document.related_document_type);
  }

  onDeleteDocument(document, event) {
    event.stopPropagation();
    const confirmDialog = this.dialog.open(ConfirmBoxComponent, {
      data: {
        type: 'dokumentą'
      }
    });
    confirmDialog.afterClosed().subscribe(confirmed => {
      if (confirmed) {
        if (document.related_document_type === 'employment_contract') {
          this.documentService.deleteEmploymentContract(document);
        } else if (document.related_document_type === 'nti_application_request') {
          this.documentService.deleteNtiApplicationRequest(document);
        } else if (document.related_document_type === 'salary_transaction_request') {
          this.documentService.deleteSalaryTransactionRequest(document);
        }
      }
    });
  }
}

export class DocumentDataSource extends DataSource<any> {
  connect(): Observable<Document[]> {
    return documentObservable;
  }

  disconnect() {}
}
