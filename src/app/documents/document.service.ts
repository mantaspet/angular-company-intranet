import { MatSnackBar } from '@angular/material';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Document } from './document.model';
import { Subject } from 'rxjs/Subject';
import { apiPath } from '../../environments/environment';

@Injectable()
export class DocumentService {
  documents: Document[];
  documentsChanged = new Subject<Document[]>();
  loading = new Subject<boolean>();

  constructor(
    private httpClient: HttpClient,
    private snackBar: MatSnackBar) { }

  getDocuments() {
    this.loading.next(true);
    this.httpClient.get(apiPath + '/documents').subscribe(
      (response: any) => {
        this.documents = response.items.data;
        this.documentsChanged.next(this.documents);
        this.loading.next(false);
      },
      (error: any) => {
        console.log(error);
        this.openSnackBar('Įvyko klaida. Kreipkitės į sistemos administratorių.', '');
      }
    );
  }

  addEmploymentContract(document: Document) {
    console.log(document);
    this.loading.next(true);
    this.httpClient.post(apiPath + '/employment-contracts', document).subscribe(
      (response: any) => {
        console.log(response);
        this.documents.push(response.item);
        this.documentsChanged.next(this.documents);
        this.openSnackBar('Dokumentas sukurtas.', '');
      },
      (error: any) => {
        console.log(error);
        this.openSnackBar('Įvyko klaida. Kreipkitės į sistemos administratorių.', '');
      }
    );
  }

  addNtiApplicationRequest(document: Document) {
    console.log(document);
    this.loading.next(true);
    this.httpClient.post(apiPath + '/nti-application-requests', document).subscribe(
      (response: any) => {
        console.log(response);
        this.documents.push(response.item);
        this.documentsChanged.next(this.documents);
        this.openSnackBar('Dokumentas sukurtas.', '');
      },
      (error: any) => {
        console.log(error);
        this.openSnackBar('Įvyko klaida. Kreipkitės į sistemos administratorių.', '');
      }
    );
  }

  addSalaryTransactionRequest(document: Document) {
    console.log(document);
    this.loading.next(true);
    this.httpClient.post(apiPath + '/salary-transaction-requests', document).subscribe(
      (response: any) => {
        console.log(response);
        this.documents.push(response.item);
        this.documentsChanged.next(this.documents);
        this.openSnackBar('Dokumentas sukurtas.', '');
      },
      (error: any) => {
        console.log(error);
        this.openSnackBar('Įvyko klaida. Kreipkitės į sistemos administratorių.', '');
      }
    );
  }

  updateEmploymentContract(document: Document) {
    console.log(document);
    this.loading.next(true);
    this.httpClient.put(apiPath + '/employment-contracts/' + document.employment_contract.id, document).subscribe(
      (response: any) => {
        console.log(response);
        this.openSnackBar('Dokumentas atnaujintas.', '');
      },
      (error: any) => {
        console.log(error);
        this.openSnackBar('Įvyko klaida. Kreipkitės į sistemos administratorių.', '');
      }
    );
  }

  updateNtiApplicationRequest(document: Document) {
    console.log(document);
    this.loading.next(true);
    this.httpClient.put(apiPath + '/nti-application-requests/' + document.nti_application_request.id, document).subscribe(
      (response: any) => {
        console.log(response);
        this.openSnackBar('Dokumentas atnaujintas.', '');
      },
      (error: any) => {
        console.log(error);
        this.openSnackBar('Įvyko klaida. Kreipkitės į sistemos administratorių.', '');
      }
    );
  }

  updateSalaryTransactionRequest(document: Document) {
    console.log(document);
    this.loading.next(true);
    this.httpClient.put(apiPath + '/salary-transaction-requests/' + document.salary_transaction_request.id, document).subscribe(
      (response: any) => {
        console.log(response);
        this.openSnackBar('Dokumentas atnaujintas.', '');
      },
      (error: any) => {
        console.log(error);
        this.openSnackBar('Įvyko klaida. Kreipkitės į sistemos administratorių.', '');
      }
    );
  }

  deleteEmploymentContract(document: Document) {
    this.loading.next(true);
    this.httpClient.delete(apiPath + '/employment-contracts/' + document.employment_contract.id).subscribe(
      () => {
        const index = this.documents.indexOf(document);
        if (index >= 0) {
          this.documents.splice(index, 1);
          this.documentsChanged.next(this.documents);
          this.openSnackBar('Dokumentas ištrintas.', '');
        }
      },
      (error: any) => {
        console.log(error);
        this.openSnackBar('Įvyko klaida. Kreipkitės į sistemos administratorių.', '');
      }
    );
  }

  deleteNtiApplicationRequest(document: Document) {
    this.loading.next(true);
    this.httpClient.delete(apiPath + '/nti-application-requests/' + document.nti_application_request.id).subscribe(
      () => {
        const index = this.documents.indexOf(document);
        if (index >= 0) {
          this.documents.splice(index, 1);
          this.documentsChanged.next(this.documents);
          this.openSnackBar('Dokumentas ištrintas.', '');
        }
      },
      (error: any) => {
        console.log(error);
        this.openSnackBar('Įvyko klaida. Kreipkitės į sistemos administratorių.', '');
      }
    );
  }

  deleteSalaryTransactionRequest(document: Document) {
    this.loading.next(true);
    this.httpClient.delete(apiPath + '/salary-transaction-requests/' + document.salary_transaction_request.id).subscribe(
      () => {
        const index = this.documents.indexOf(document);
        if (index >= 0) {
          this.documents.splice(index, 1);
          this.documentsChanged.next(this.documents);
          this.openSnackBar('Dokumentas ištrintas.', '');
        }
      },
      (error: any) => {
        console.log(error);
        this.openSnackBar('Įvyko klaida. Kreipkitės į sistemos administratorių.', '');
      }
    );
  }

  createPDF(id: number, url: string, type: string) {
    if (type === 'employment_contract') {
      window.open(apiPath + '/employment-contracts/' + id + '/create-pdf/' + url);
    } else if (type === 'nti_application_request') {
      window.open(apiPath + '/nti-application-requests/' + id + '/create-pdf/' + url);
    } else if (type === 'salary_transaction_request') {
      window.open(apiPath + '/salary-transaction-requests/' + id + '/create-pdf/' + url);
    }
  }

  openSnackBar(message: string, action: string) {
    this.loading.next(false);
    this.snackBar.open(message, action, {
      duration: 2500,
    });
  }
}
