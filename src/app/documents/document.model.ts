import { SalaryTransactionRequest } from './salary-transaction-request-edit/salary-transaction-request.model';
import { EmploymentContract } from './employment-contract-edit/employment-contract.model';
import { NtiApplicationRequest } from './nti-application-request-edit/nti-application-request.model';

export class Document {
    id: number;
    title: string;
    url: string;
    related_document_id: number;
    related_document_type: string;
    employee_id: number;
    employment_contract: EmploymentContract;
    nti_application_request: NtiApplicationRequest;
    salary_transaction_request: SalaryTransactionRequest;
}