export class SalaryTransactionRequest {
    id: number;
    document_id: number;
    position: string;
    first_name: string;
    last_name: string;
    company_name: string;
    company_representative: string;
    date: Date;
    bank: string;
    bank_account_number: string;
}