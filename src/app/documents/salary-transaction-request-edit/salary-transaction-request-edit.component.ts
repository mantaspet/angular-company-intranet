import { DatePipe } from '@angular/common';
import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { DocumentService } from '../document.service';
import { MatDialogRef } from '@angular/material';
import { MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-salary-transaction-request-edit',
  templateUrl: './salary-transaction-request-edit.component.html',
  styleUrls: ['./salary-transaction-request-edit.component.css']
})
export class SalaryTransactionRequestEditComponent implements OnInit {
  wasChanged = false;
  @ViewChild('f') documentForm: NgForm;

  constructor(
    private documentService: DocumentService,
    private datePipe: DatePipe,
    public dialogRef: MatDialogRef<SalaryTransactionRequestEditComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
    if (this.data.editMode) {
      const tmpDocument = JSON.parse(JSON.stringify(this.data.document));
      console.log(tmpDocument);
      this.dialogRef.beforeClose().subscribe(
        () => {
          if (!this.wasChanged) {
            console.log(tmpDocument);
            this.documentForm.setValue({
              title: tmpDocument.title,
              position: tmpDocument.salary_transaction_request.position,
              first_name: tmpDocument.salary_transaction_request.first_name,
              last_name: tmpDocument.salary_transaction_request.last_name,
              company_name: tmpDocument.salary_transaction_request.company_name,
              company_representative: tmpDocument.salary_transaction_request.company_representative,
              date: tmpDocument.salary_transaction_request.date,
              bank: tmpDocument.salary_transaction_request.bank,
              bank_account_number: tmpDocument.salary_transaction_request.bank_account_number
            });
          }
        }
      );
    }
  }

  onSubmitDocument() {
    if (!this.data.editMode) {
      this.data.document.salary_transaction_request.date =
        this.datePipe.transform(this.data.document.salary_transaction_request.date, 'yyyy-MM-dd');
      this.documentService.addSalaryTransactionRequest(this.data.document);
      this.wasChanged = true;
    } else {
      this.data.document.salary_transaction_request.date =
        this.datePipe.transform(this.data.document.salary_transaction_request.date, 'yyyy-MM-dd');
      this.documentService.updateSalaryTransactionRequest(this.data.document);
      this.wasChanged = true;
    }
    this.dialogRef.close();
  }
  
  onClear() {
    this.documentForm.reset();
  }

}
