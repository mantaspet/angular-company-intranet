import { DatePipe } from '@angular/common';
import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { DocumentService } from '../document.service';
import { MatDialogRef } from '@angular/material';
import { MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-employment-contract-edit',
  templateUrl: './employment-contract-edit.component.html',
  styleUrls: ['./employment-contract-edit.component.css']
})
export class EmploymentContractEditComponent implements OnInit {
  wasChanged = false;
  @ViewChild('f') documentForm: NgForm;

  constructor(
    private documentService: DocumentService,
    private datePipe: DatePipe,
    public dialogRef: MatDialogRef<EmploymentContractEditComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
    if (this.data.editMode) {
      const tmpDocument = JSON.parse(JSON.stringify(this.data.document));
      console.log(tmpDocument);
      this.dialogRef.beforeClose().subscribe(
        () => {
          if (!this.wasChanged) {
            console.log(tmpDocument);
            this.documentForm.setValue({
              title: tmpDocument.title,
              date: tmpDocument.employment_contract.date,
              registration_number: tmpDocument.employment_contract.registration_number,
              employer: tmpDocument.employment_contract.employer,
              employer_code: tmpDocument.employment_contract.employer_code,
              employer_representative: tmpDocument.employment_contract.employer_representative,
              employer_representative_nin: tmpDocument.employment_contract.employer_representative_nin,
              employee_first_name: tmpDocument.employment_contract.employee_first_name,
              employee_last_name: tmpDocument.employment_contract.employee_last_name,
              employee_identity_card_number: tmpDocument.employment_contract.employee_identity_card_number,
              employee_identity_card_issuance_date: tmpDocument.employment_contract.employee_identity_card_issuance_date,
              employee_nin: tmpDocument.employment_contract.employee_nin,
              employee_address: tmpDocument.employment_contract.employee_address,
              employee_position: tmpDocument.employment_contract.employee_position,
              employee_salary: tmpDocument.employment_contract.employee_salary,
              employee_salary_comment:
                tmpDocument.employment_contract.employee_salary_comment ? tmpDocument.employment_contract.employee_salary_comment : '',
              valid_from: tmpDocument.employment_contract.valid_from,
              started_from: tmpDocument.employment_contract.started_from
            });
          }
        }
      );
    }
  }

  onSubmitDocument() {
    if (!this.data.editMode) {
      this.transformDates();
      this.documentService.addEmploymentContract(this.data.document);
      this.wasChanged = true;
    } else {
      this.transformDates();
      this.documentService.updateEmploymentContract(this.data.document);
      this.wasChanged = true;
    }
    this.dialogRef.close();
  }

  transformDates() {
    this.data.document.employment_contract.date =
      this.datePipe.transform(this.data.document.employment_contract.date, 'yyyy-MM-dd');
    this.data.document.employment_contract.employee_identity_card_issuance_date =
      this.datePipe.transform(this.data.document.employment_contract.employee_identity_card_issuance_date, 'yyyy-MM-dd');
    this.data.document.employment_contract.valid_from =
      this.datePipe.transform(this.data.document.employment_contract.valid_from, 'yyyy-MM-dd');
    this.data.document.employment_contract.started_from =
      this.datePipe.transform(this.data.document.employment_contract.started_from, 'yyyy-MM-dd');
  }

  onClear() {
    this.documentForm.reset();
  }

}
