export class EmploymentContract {
    id: number;
    document_id: number;
    date: Date;
    registration_number: number;
    employer: string;
    employer_code: string;
    employer_representative: string;
    employer_representative_nin: string;
    employee_first_name: string;
    employee_last_name: string;
    employee_identity_card_number: string;
    employee_identity_card_issuance_Date: Date;
    employee_nin: string;
    employee_address: string;
    employee_position: string;
    employee_salary: number;
    employee_salary_comment: string;
    valid_from: Date;
    started_from: Date;
}