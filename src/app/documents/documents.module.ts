import { EmploymentContractEditComponent } from './employment-contract-edit/employment-contract-edit.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { DocumentService } from './document.service';
import { DocumentListComponent } from './document-list/document-list.component';
import {
  MatTableModule, MatToolbarModule, MatButtonModule, MatInputModule, MatFormFieldModule, MatCardModule, MatDialogModule,
  MatSnackBarModule, MatIconModule, MatDatepickerModule, MatTooltipModule, MatProgressBarModule
} from '@angular/material';
import { NtiApplicationRequestEditComponent } from './nti-application-request-edit/nti-application-request-edit.component';
import { SalaryTransactionRequestEditComponent } from './salary-transaction-request-edit/salary-transaction-request-edit.component';
import { ConfirmBoxComponent } from '../shared/confirm-box/confirm-box.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatCardModule,
    MatButtonModule,
    MatTableModule,
    MatToolbarModule,
    MatDialogModule,
    MatSnackBarModule,
    MatIconModule,
    MatDatepickerModule,
    MatTooltipModule,
    MatProgressBarModule
  ],
  declarations: [
    DocumentListComponent,
    EmploymentContractEditComponent,
    NtiApplicationRequestEditComponent,
    SalaryTransactionRequestEditComponent
  ],
  providers: [DocumentService],
  entryComponents: [
    ConfirmBoxComponent,
    EmploymentContractEditComponent,
    NtiApplicationRequestEditComponent,
    SalaryTransactionRequestEditComponent
  ]
})
export class DocumentsModule { }
