import { Component, OnInit } from '@angular/core';
import { DataSource } from '@angular/cdk/collections';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import { Department } from '../department.model';
import { DepartmentService } from '../department.service';
import { MatDialog } from '@angular/material';
import { DepartmentEditComponent } from '../department-edit/department-edit.component';

let departmentObservable;

@Component({
  selector: 'app-department-list',
  styleUrls: ['department-list.component.css'],
  templateUrl: 'department-list.component.html',
})
export class DepartmentListComponent implements OnInit {
  displayedColumns = ['name', 'country', 'city', 'address']; // 'delete'
  dataSource = new DepartmentDataSource();
  loading = false;

  constructor(
    private departmentService: DepartmentService,
    public dialog: MatDialog) {}

  ngOnInit() {
    this.departmentService.loading.subscribe(
      (status: boolean) => {
        this.loading = status;
      }
    );
    departmentObservable = this.departmentService.departmentsChanged;
    this.departmentService.getDepartments();
  }

  onNewDepartment(): void {
    const departmentDialog = this.dialog.open(DepartmentEditComponent, {
      data: {
        editMode: false,
        department: {
          name: '',
          country: '',
          city: '',
          address: ''
        }
      }
    });
  }

  onUpdateDepartment(department: Department) {
    const departmentDialog = this.dialog.open(DepartmentEditComponent, {
      data: {
        editMode: true,
        department: department
      }
    });
  }

  // onDeleteDepartment(department, event) {
  //   event.stopPropagation();
  //   const confirmDialog = this.dialog.open(ConfirmBoxComponent, {
  //     data: {
  //       type: 'padalinį'
  //     }
  //   });
  //   confirmDialog.afterClosed().subscribe(confirmed => {
  //     if (confirmed) {
  //       this.departmentService.deleteDepartment(department);
  //     }
  //   });
  // }
}

export class DepartmentDataSource extends DataSource<any> {
  connect(): Observable<Department[]> {
    return departmentObservable;
  }

  disconnect() {}
}
