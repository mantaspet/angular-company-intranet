export class Department {
  id: number;
  name: string;
  country: string;
  city: string;
  address: string;
}
