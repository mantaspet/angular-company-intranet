import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Department } from './department.model';
import { Subject } from 'rxjs/Subject';
import { apiPath } from '../../environments/environment';
import { MatSnackBar } from '@angular/material';

@Injectable()
export class DepartmentService {
  departments: Department[];
  departmentsChanged = new Subject<Department[]>();
  loading = new Subject<boolean>();

  constructor(
    private httpClient: HttpClient,
    private snackBar: MatSnackBar) { }

  getDepartments() {
    this.loading.next(true);
    this.httpClient.get(apiPath + '/departments', {}).subscribe(
      (response: any) => {
        this.departments = response.items.data;
        this.departmentsChanged.next(this.departments);
        this.loading.next(false);
      },
      (error: any) => {
        console.log(error);
        this.openSnackBar('Įvyko klaida. Kreipkitės į sistemos administratorių.', '');
      }
    );
  }

  addDepartment(department: Department) {
    this.loading.next(true);
    this.httpClient.post(apiPath + '/departments', department).subscribe(
      (response: any) => {
        this.departments.push(response.item);
        this.departmentsChanged.next(this.departments);
        this.openSnackBar('Padalinys sukurtas.', '');
      },
      (error: any) => {
        console.log(error);
        this.openSnackBar('Įvyko klaida. Kreipkitės į sistemos administratorių.', '');
      }
    );
  }

  updateDepartment(department: Department) {
    this.loading.next(true);
    this.httpClient.put(apiPath + '/departments/' + department.id, department).subscribe(
      () => {
        this.openSnackBar('Padalinys atnaujintas.', '');
      },
      (error: any) => {
        console.log(error);
        this.openSnackBar('Įvyko klaida. Kreipkitės į sistemos administratorių.', '');
      }
    );
  }

  // deleteDepartment(department: Department) {
  //   this.loading.next(true);
  //   this.httpClient.delete(apiPath + '/departments/' + department.id).subscribe(
  //     () => {
  //       const index = this.departments.indexOf(department);
  //       if (index >= 0) {
  //         this.departments.splice(index, 1);
  //         this.departmentsChanged.next(this.departments);
  //         this.openSnackBar('Padalinys ištrintas.', '');
  //       }
  //     },
  //     (error: any) => {
  //       console.log(error);
  //       this.openSnackBar('Įvyko klaida. Kreipkitės į sistemos administratorių.', '');
  //     }
  //   );
  // }

  openSnackBar(message: string, action: string) {
    this.loading.next(false);
    this.snackBar.open(message, action, {
      duration: 2500,
    });
  }
}
