import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DepartmentEditComponent } from './department-edit/department-edit.component';
import { FormsModule } from '@angular/forms';
import { DepartmentService } from './department.service';
import { DepartmentListComponent } from './department-list/department-list.component';
import {
  MatTableModule, MatToolbarModule, MatButtonModule, MatInputModule, MatFormFieldModule, MatCardModule, MatDialogModule,
  MatSnackBarModule, MatIconModule, MatTooltipModule, MatProgressBarModule
} from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatCardModule,
    MatButtonModule,
    MatTableModule,
    MatToolbarModule,
    MatDialogModule,
    MatSnackBarModule,
    MatIconModule,
    MatTooltipModule,
    MatProgressBarModule
  ],
  declarations: [DepartmentEditComponent, DepartmentListComponent],
  providers: [DepartmentService],
  entryComponents: [DepartmentEditComponent]
})
export class DepartmentsModule { }
