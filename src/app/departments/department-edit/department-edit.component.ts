import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { DepartmentService } from '../department.service';
import { MatDialogRef } from '@angular/material';
import { MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-department-edit',
  templateUrl: './department-edit.component.html',
  styleUrls: ['./department-edit.component.css']
})
export class DepartmentEditComponent implements OnInit {
  editMode = false;
  wasChanged = false;
  @ViewChild('f') departmentForm: NgForm;

  constructor(
    private departmentService: DepartmentService,
    public dialogRef: MatDialogRef<DepartmentEditComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
    if (this.data.editMode) {
      this.editMode = this.data.editMode;
      const tmpDepartment = {...this.data.department};
      this.dialogRef.beforeClose().subscribe(
        () => {
          if (!this.wasChanged) {
            this.departmentForm.setValue({
              name: tmpDepartment.name,
              country: tmpDepartment.country,
              city: tmpDepartment.city,
              address: tmpDepartment.address
            });
          }
        }
      );
    }
  }

  onSubmitDepartment(form: NgForm, department: any) {
    if (this.editMode) {
      this.departmentService.updateDepartment(department);
      this.wasChanged = true;
    } else {
      this.departmentService.addDepartment(form.value);
      this.wasChanged = true;
    }
    this.editMode = false;
    console.log(form);
    this.dialogRef.close();
  }

  onClear() {
    this.departmentForm.reset();
  }

}
