import { OfficeService } from './../../offices/office.service';
import { Component, Inject, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { EmployeeService } from '../employee.service';
import { MatDialogRef } from '@angular/material';
import { MAT_DIALOG_DATA } from '@angular/material';
import { Office } from '../../offices/office.model';
import { Department } from '../../departments/department.model';
import { Subscription } from 'rxjs/Subscription';
import { DepartmentService } from '../../departments/department.service';
import { PositionService } from '../../positions/position.service';
import { Position } from '../../positions/position.model';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-employee-edit',
  templateUrl: './employee-edit.component.html',
  styleUrls: ['./employee-edit.component.css']
})
export class EmployeeEditComponent implements OnInit, OnDestroy {
  editMode = false;
  wasChanged = false;
  @ViewChild('f') employeeForm: NgForm;
  minDate = new Date(1900, 0, 1);
  maxDate = new Date(2017, 0, 1);

  officesSubscription: Subscription;
  positionsSubscription: Subscription;
  departmentsSubscription: Subscription;

  offices: Office[];
  positions: Position[];
  departments: Department[];
  filteredOffices: Office[];
  filteredPositions: Position[];
  filteredDepartments: Department[];

  constructor(
    private employeeService: EmployeeService,
    private officeService: OfficeService,
    private departmentService: DepartmentService,
    private positionService: PositionService,
    public dialogRef: MatDialogRef<EmployeeEditComponent>,
    private datePipe: DatePipe,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
    this.getOfficesList();
    this.getDepartmentsList();
    this.getPositionsList();
    if (this.data.editMode) {
      this.editMode = this.data.editMode;
      const tmpEmployee = JSON.parse(JSON.stringify(this.data.employee));
      this.dialogRef.beforeClose().subscribe(
        () => {
          if (!this.wasChanged) {
            this.employeeForm.setValue({
              first_name: tmpEmployee.contact.first_name,
              last_name: tmpEmployee.contact.last_name,
              position: tmpEmployee.employee_history[0].position.name,
              status: tmpEmployee.contact.status,
              gender: tmpEmployee.contact.gender,
              dob: tmpEmployee.contact.dob,
              email: tmpEmployee.contact.email,
              phone: tmpEmployee.contact.phone,
              nin: tmpEmployee.contact.nin,
              salary: tmpEmployee.employee_salary_history[0].salary,
              office: tmpEmployee.office.name,
              department: tmpEmployee.department.name,
              country: tmpEmployee.contact.country,
              city: tmpEmployee.contact.city,
              address: tmpEmployee.contact.address
            });
          }
        }
      );
    }
  }

  ngOnDestroy() {
    this.officesSubscription.unsubscribe();
    this.departmentsSubscription.unsubscribe();
    this.positionsSubscription.unsubscribe();
  }

  onSubmitEmployee(form: NgForm, employee: any) {
    if (this.editMode) {
      employee = this.alterDataStructure(form, employee);
      this.employeeService.updateEmployee(employee);
      this.wasChanged = true;
    } else {
      employee = this.alterDataStructure(form, employee);
      this.employeeService.addEmployee(employee);
      this.wasChanged = true;
    }
    this.editMode = false;
    this.dialogRef.close();
  }

  onClear() {
    this.employeeForm.reset();
  }

  alterDataStructure(form: NgForm, employee: any) {
    employee.office_id = this.offices.find(office => office.name === form.value.office).id;
    employee.department_id = this.departments.find(department => department.name === form.value.department).id;
    employee.position_id = this.positions.find(position => position.name === form.value.position).id;
    this.datePipe.transform(employee.contact.dob, 'yyyy-MM-dd');
    if (!this.isValidDate(employee.contact.dob.toString())) {
      const day = employee.contact.dob.getDate();
      const month = employee.contact.dob.getMonth() + 1;
      const year = employee.contact.dob.getFullYear();
      employee.contact.dob = year + '-' + month + '-' + day;
    }
    return employee;
  }

  isValidDate(dateString: string) {
    const regEx = /^\d{4}-\d{2}-\d{2}$/;
    if (!dateString.match(regEx)) {
      return false;  // Invalid format
    }
    const d = new Date(dateString);
    if (!d.getTime()) {
      return false; // Invalid date (or this could be epoch)
    }
    return d.toISOString().slice(0, 10) === dateString;
  }

  getOfficesList() {
    this.officesSubscription = this.officeService.officesChanged.subscribe(
      (response: any) => {
        this.offices = response;
        this.filteredOffices = this.offices;
        console.log(this.offices);
      }
    );
    this.officeService.getOffices();
  }

  getDepartmentsList() {
    this.departmentsSubscription = this.departmentService.departmentsChanged.subscribe(
      (response: any) => {
        this.departments = response;
        this.filteredDepartments = this.departments;
      }
    );
    this.departmentService.getDepartments();
  }

  getPositionsList() {
    this.positionsSubscription = this.positionService.positionsChanged.subscribe(
      (response: any) => {
        this.positions = response;
        this.filteredPositions = this.positions;
      }
    );
    this.positionService.getPositions();
  }

  filterOffices(name: string) {
    this.filteredOffices = this.offices.filter(office =>
      office.name.toLowerCase().indexOf(name.toLowerCase()) === 0);
  }

  filterDepartments(name: string) {
    this.filteredDepartments = this.departments.filter(office =>
      office.name.toLowerCase().indexOf(name.toLowerCase()) === 0);
  }

  filterPositions(name: string) {
    this.filteredPositions = this.positions.filter((position: Position) =>
      position.name.toLowerCase().indexOf(name.toLowerCase()) === 0);
  }

}
