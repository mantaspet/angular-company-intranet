export class Contact {
  id: number;
  nin: string;
  first_name: string;
  last_name: string;
  email: string;
  phone: string;
  status: string;
  dob: Date;
  gender: string;
  country: string;
  city: string;
  address: string;
}
