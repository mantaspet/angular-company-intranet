export class SalaryHistory {
    id: number;
    salary: number;
    from: Date;
    to: Date;
    is_fixed: boolean;
    variable_part_description: string;
    employee_id: number;
}
