import { Component, OnInit } from '@angular/core';
import { DataSource } from '@angular/cdk/collections';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import { SalaryHistory } from '../salary-history.model';
import { SalaryHistoryService } from '../salary-history.service';
import { MatDialog } from '@angular/material';
import { SalaryHistoryEditComponent } from '../salary-history-edit/salary-history-edit.component';
import { ActivatedRoute, Params } from '@angular/router';

let salaryHistoryObservable;

@Component({
  selector: 'app-salary-history-list',
  styleUrls: ['salary-history-list.component.css'],
  templateUrl: 'salary-history-list.component.html',
})
export class SalaryHistoryListComponent implements OnInit {
  employee_id: number;
  first_name: string;
  last_name: string;
  displayedColumns = ['salary', 'from', 'to', 'variable_part_description'];
  dataSource = new SalaryHistoryDataSource();
  loading = false;
  my_history = false;

  constructor(
    private salaryHistoryService: SalaryHistoryService,
    private route: ActivatedRoute,
    public dialog: MatDialog) {}

  ngOnInit() {
    this.salaryHistoryService.loading.subscribe(
      (status: boolean) => {
        this.loading = status;
      }
    );
    salaryHistoryObservable = this.salaryHistoryService.salaryHistoryChanged;
    this.route.params.subscribe(
      (params: Params) => {
        if (params['page'] === 'my_history') {
          this.my_history = true;
          this.salaryHistoryService.getMySalaryHistory();
        } else {
          this.employee_id = +params['id'];
          this.first_name = params['first_name'];
          this.last_name = params['last_name'];
          this.salaryHistoryService.getSalaryHistory(this.employee_id);
        }
      }
    );
  }

  onNewSalaryHistory(): void {
    const salaryHistoryDialog = this.dialog.open(SalaryHistoryEditComponent, {
      data: {
        editMode: false,
        employee_id: this.employee_id,
        employee_salary_history: {
          salary: '',
          from: '',
          to: '',
          is_fixed: '',
          variable_part_description: ''
        }
      }
    });
  }

  onUpdateSalaryHistory(employee_salary_history: SalaryHistory) {
    const salaryHistoryDialog = this.dialog.open(SalaryHistoryEditComponent, {
      data: {
        editMode: true,
        my_history: this.my_history,
        employee_id: this.employee_id,
        employee_salary_history: employee_salary_history
      }
    });
  }
}

export class SalaryHistoryDataSource extends DataSource<any> {
  connect(): Observable<SalaryHistory[]> {
    return salaryHistoryObservable;
  }

  disconnect() {}
}
