import { DatePipe } from '@angular/common';
import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { SalaryHistoryService } from '../salary-history.service';
import { MatDialogRef } from '@angular/material';
import { MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-salary-history-edit',
  templateUrl: './salary-history-edit.component.html',
  styleUrls: ['./salary-history-edit.component.css']
})
export class SalaryHistoryEditComponent implements OnInit {
  editMode = false;
  wasChanged = false;
  employee_id: number;
  @ViewChild('f') salaryHistoryForm: NgForm;

  constructor(
    private salaryHistoryService: SalaryHistoryService,
    private datePipe: DatePipe,
    public dialogRef: MatDialogRef<SalaryHistoryEditComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
    this.employee_id = this.data.employee_id;
    if (this.data.editMode) {
      this.editMode = this.data.editMode;
      const tmpSalaryHistory = {...this.data.employee_salary_history};
      this.dialogRef.beforeClose().subscribe(
        () => {
          if (!this.wasChanged) {
            if (this.salaryHistoryForm.value['is_fixed']) {
              this.salaryHistoryForm.setValue({
                salary: tmpSalaryHistory.salary,
                from: tmpSalaryHistory.from,
                to: tmpSalaryHistory.to,
                is_fixed: tmpSalaryHistory.is_fixed
              });
            } else {
              this.salaryHistoryForm.setValue({
                salary: tmpSalaryHistory.salary,
                from: tmpSalaryHistory.from,
                to: tmpSalaryHistory.to,
                is_fixed: tmpSalaryHistory.is_fixed,
                variable_part_description: tmpSalaryHistory.variable_part_description ? tmpSalaryHistory.variable_part_description : ''
              });
            }
          }
        }
      );
    }
  }

  onSubmitSalaryHistory(form: NgForm, employee_salary_history: any) {
    if (this.editMode) {
      this.alterDataStructure(employee_salary_history);
      this.salaryHistoryService.updateSalaryHistory(employee_salary_history);
      this.wasChanged = true;
    } else {
      this.alterDataStructure(employee_salary_history);
      employee_salary_history.employee_id = this.employee_id;
      this.salaryHistoryService.addSalaryHistory(employee_salary_history);
      this.wasChanged = true;
    }
    this.editMode = false;
    this.dialogRef.close();
  }

  alterDataStructure(employee_salary_history: any) {
    this.datePipe.transform(employee_salary_history.from, 'yyyy-MM-dd');
    this.datePipe.transform(employee_salary_history.to, 'yyyy-MM-dd');
    if (!this.isValidDate(employee_salary_history.from.toString())) {
      const day = employee_salary_history.from.getDate();
      const month = employee_salary_history.from.getMonth() + 1;
      const year = employee_salary_history.from.getFullYear();
      employee_salary_history.from = year + '-' + month + '-' + day;
    }
    if (employee_salary_history.to && !this.isValidDate(employee_salary_history.to.toString())) {
      const day = employee_salary_history.to.getDate();
      const month = employee_salary_history.to.getMonth() + 1;
      const year = employee_salary_history.to.getFullYear();
      employee_salary_history.to = year + '-' + month + '-' + day;
    }
    if (employee_salary_history.is_fixed) {
      employee_salary_history.variable_part_description = '';
    } else {
      employee_salary_history.is_fixed = false;
    }
    return employee_salary_history;
  }

  isValidDate(dateString: string) {
    const regEx = /^\d{4}-\d{2}-\d{2}$/;
    if (!dateString.match(regEx)) {
      return false;  // Invalid format
    }
    const d = new Date(dateString);
    if (!d.getTime()) {
      return false; // Invalid date (or this could be epoch)
    }
    return d.toISOString().slice(0, 10) === dateString;
  }

  onClear() {
    this.salaryHistoryForm.reset();
  }

}
