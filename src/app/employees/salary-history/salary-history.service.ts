import { MatSnackBar } from '@angular/material';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SalaryHistory } from './salary-history.model';
import { Subject } from 'rxjs/Subject';
import { apiPath } from '../../../environments/environment';

@Injectable()
export class SalaryHistoryService {
  employee_salary_history: SalaryHistory[];
  salaryHistoryChanged = new Subject<SalaryHistory[]>();
  loading = new Subject<boolean>();

  constructor(
    private httpClient: HttpClient,
    private snackBar: MatSnackBar) { }

  getSalaryHistory(employee_id: number) {
    this.loading.next(true);
    this.httpClient.get(apiPath + '/employee-salary/' + employee_id, {}).subscribe(
      (response: any) => {
        this.employee_salary_history = response.item;
        this.salaryHistoryChanged.next(this.employee_salary_history);
        this.loading.next(false);
      },
      (error: any) => {
        console.log(error);
        this.openSnackBar('Įvyko klaida. Kreipkitės į sistemos administratorių.', '');
      }
    );
  }

  getMySalaryHistory() {
    this.loading.next(true);
    this.httpClient.get(apiPath + '/my-salary-history/').subscribe(
      (response: any) => {
        this.employee_salary_history = response.item;
        this.salaryHistoryChanged.next(this.employee_salary_history);
        this.loading.next(false);
      },
      (error: any) => {
        console.log(error);
        this.openSnackBar('Įvyko klaida. Kreipkitės į sistemos administratorių.', '');
      }
    );
  }

  addSalaryHistory(employee_salary_history: SalaryHistory) {
    this.loading.next(true);
    this.httpClient.post(apiPath + '/employee-salary', employee_salary_history).subscribe(
      (response: any) => {
        this.employee_salary_history.push(response.item);
        this.salaryHistoryChanged.next(this.employee_salary_history);
        this.openSnackBar('Atlyginimo įrašas sukurtas.', '');
      },
      (error: any) => {
        console.log(error);
        this.openSnackBar('Įvyko klaida. Kreipkitės į sistemos administratorių.', '');
      }
    );
  }

  updateSalaryHistory(employee_salary_history: SalaryHistory) {
    this.loading.next(true);
    this.httpClient.put(apiPath + '/employee-salary/' + employee_salary_history.id, employee_salary_history).subscribe(
      () => {
        this.openSnackBar('Atlyginimo įrašas atnaujintas.', '');
      },
      (error: any) => {
        console.log(error);
        this.openSnackBar('Įvyko klaida. Kreipkitės į sistemos administratorių.', '');
      }
    );
  }

  openSnackBar(message: string, action: string) {
    this.loading.next(false);
    this.snackBar.open(message, action, {
      duration: 2500,
    });
  }
}
