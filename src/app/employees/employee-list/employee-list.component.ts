import { Component, OnInit } from '@angular/core';
import { DataSource } from '@angular/cdk/collections';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import { Employee } from '../employee.model';
import { EmployeeService } from '../employee.service';
import { MatDialog } from '@angular/material';
import { EmployeeEditComponent } from '../employee-edit/employee-edit.component';
import { Router } from '@angular/router';
import { SlicePipe } from '@angular/common';

let employeeObservable;

@Component({
  selector: 'app-employee-list',
  styleUrls: ['employee-list.component.css'],
  templateUrl: 'employee-list.component.html',
})
export class EmployeeListComponent implements OnInit {
  displayedColumns = ['first_name', 'last_name', 'phone', 'position', 'status', 'delete'];
  dataSource = new EmployeeDataSource();
  loading = false;

  constructor(
    private employeeService: EmployeeService,
    private router: Router,
    public dialog: MatDialog) {}

  ngOnInit() {
    this.employeeService.loading.subscribe(
      (status: boolean) => {
        this.loading = status;
      }
    );
    this.employeeService.getEmployees();
    employeeObservable = this.employeeService.employeesChanged;
  }

  onNewEmployee(): void {
    const employeeDialog = this.dialog.open(EmployeeEditComponent, {
      data: {
        editMode: false,
        employee: {
          office: '',
          department: '',
          contact: {
            first_name: '',
            last_name: '',
            status: '',
            gender: '',
            email: '',
            phone: '',
            dob: '',
            nin: '',
            country: '',
            city: '',
            address: ''
          },
          employee_history: [{
            position: {
              name: ''
            }
          }],
          employee_salary_history: [{
            salary: ''
          }]
        }
      }
    });
  }

  onUpdateEmployee(employee: Employee) {
    console.log(employee);
    if (employee.employee_history.length === 0) {
      employee.employee_history.push({
        id: 0,
        from: null,
        to: null,
        position_id: 0,
        employee_id: employee.id,
        position: {
          id: 0,
          name: 'Nepriskirta'
        }
      });
    }
    if (employee.employee_salary_history.length === 0) {
      employee.employee_salary_history.push({
        id: 0,
        salary: 0,
        from: null,
        to: null,
        is_fixed: true,
        variable_part_description: '',
        employee_id: employee.id,
      });
    }
    const employeeDialog = this.dialog.open(EmployeeEditComponent, {
      data: {
        editMode: true,
        salaryChanged: false,
        positionChanged: false,
        employee: employee
      }
    });
  }

  showHistory(employee: Employee, type: string) {
    this.router.navigate(['employees', employee.id, type, {
      first_name: employee.contact.first_name,
      last_name: employee.contact.last_name
    } ]);
  }

  createAccount(employee: Employee, role: string) {
    this.employeeService.signupUser(employee, role);
  }

  suspendAccount(employee: Employee) {
    this.employeeService.suspendAccount(employee);
  }

  resetPassword(employee: Employee) {
    this.employeeService.resetPassword(employee);
  }

  // onDeleteEmployee(employee, event) {
  //   event.stopPropagation();
  //   const confirmDialog = this.dialog.open(ConfirmBoxComponent, {
  //     data: {
  //       type: 'darbuotoją'
  //     }
  //   });
  //   confirmDialog.afterClosed().subscribe(confirmed => {
  //     if (confirmed) {
  //       this.employeeService.deleteEmployee(employee);
  //     }
  //   });
  // }
}

export class EmployeeDataSource extends DataSource<any> {
  connect(): Observable<Employee[]> {
    return employeeObservable;
  }

  disconnect() {}
}
