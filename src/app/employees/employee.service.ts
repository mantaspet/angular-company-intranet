import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Employee } from './employee.model';
import { Subject } from 'rxjs/Subject';
import { apiPath } from '../../environments/environment';
import { MatSnackBar } from '@angular/material';

@Injectable()
export class EmployeeService {
  employees: Employee[];
  employeesChanged = new Subject<Employee[]>();
  loading = new Subject<boolean>();

  constructor(
    private httpClient: HttpClient,
    private snackBar: MatSnackBar) { }

  getEmployees() {
    this.loading.next(true);
    this.httpClient.get(apiPath + '/employees').subscribe(
      (response: any) => {
        this.employees = response.items.data;
        this.employeesChanged.next(this.employees);
        this.loading.next(false);
      },
      (error: any) => {
        console.log(error);
        this.openSnackBar('Įvyko klaida. Kreipkitės į sistemos administratorių.', '');
      }
    );
  }

  signupUser(employee: Employee, role: string) {
    this.loading.next(true);
    const requestBody = {
      role: role,
      employee_id: employee.id,
      email: employee.contact.email,
      contact_id: employee.contact_id,
      password: 'abc123'
    };
    console.log(requestBody);
    const index = this.employees.indexOf(employee);
    this.httpClient.post(apiPath + '/users', requestBody).subscribe(
      (response: any) => {
        console.log(response);
        if (response.item.user_id) {
          this.employees[index].user_id = response.item.user_id;
          this.employeesChanged.next(this.employees);
          this.openSnackBar('Paskyra sukurta.', '');
        }
        console.log(response);
      },
      (error: any) => {
        console.log(error);
        this.openSnackBar('Įvyko klaida. Kreipkitės į sistemos administratorių.', '');
      }
    );
  }

  suspendAccount(employee: Employee) {
    this.loading.next(true);
    const requestBody = {
      employee_id: employee.id,
      email: employee.contact.email,
    };
    console.log(requestBody);
    const index = this.employees.indexOf(employee);
    this.httpClient.put(apiPath + '/suspend-user', requestBody).subscribe(
      (response: any) => {
        console.log(response);
        this.employees[index].user_id = null;
        this.employeesChanged.next(this.employees);
        this.openSnackBar('Paskyra suspenduota.', '');
      },
      (error: any) => {
        console.log(error);
        this.openSnackBar('Įvyko klaida. Kreipkitės į sistemos administratorių.', '');
      }
    );
  }

  resetPassword(employee: Employee) {
    this.loading.next(true);
    const requestBody = {
      email: employee.contact.email,
      password: 'abc123'
    };
    console.log(requestBody);
    this.httpClient.put(apiPath + '/reset-password', requestBody).subscribe(
      (response: any) => {
        console.log(response);
        this.openSnackBar('Slaptažodis atstatytas.', '');
      },
      (error: any) => {
        console.log(error);
        this.openSnackBar('Įvyko klaida. Kreipkitės į sistemos administratorių.', '');
      }
    );
  }

  addEmployee(employee: Employee) {
    this.loading.next(true);
    const newEmployeeData = {
      contact: employee.contact,
      office_id: employee.office_id,
      department_id: employee.department_id,
      position_id: employee.position_id,
      salary: employee.employee_salary_history[0].salary
    };
    this.httpClient.post(apiPath + '/employees', newEmployeeData).subscribe(
      (response: any) => {
        this.employees.push(response.item);
        this.employeesChanged.next(this.employees);
        this.openSnackBar('Darbuotojas sukurtas.', '');
      },
      (error: any) => {
        console.log(error);
        this.openSnackBar('Įvyko klaida. Kreipkitės į sistemos administratorių.', '');
      }
    );
  }

  updateEmployee(employee: Employee) {
    this.loading.next(true);
    const newEmployeeData = {
      contact: employee.contact,
      office_id: employee.office_id,
      department_id: employee.department_id,
      position_id: employee.position_id,
      salary: employee.employee_salary_history[0].salary
    };
    this.httpClient.put(apiPath + '/employees/' + employee.id, newEmployeeData).subscribe(
      (response: any) => {
        const index = this.employees.indexOf(employee);
        if (index >= 0) {
          this.employees[index] = response.item;
          this.employeesChanged.next(this.employees);
          this.openSnackBar('Darbuotojas atnaujintas.', '');
        }
      },
      (error: any) => {
        console.log(error);
        this.openSnackBar('Įvyko klaida. Kreipkitės į sistemos administratorių.', '');
      }
    );
  }

  deleteEmployee(employee: Employee) {
    this.loading.next(true);
    this.httpClient.delete(apiPath + '/employees/' + employee.id).subscribe(
      () => {
        const index = this.employees.indexOf(employee);
        if (index >= 0) {
          this.employees.splice(index, 1);
          this.employeesChanged.next(this.employees);
          this.openSnackBar('Darbuotojas ištrintas.', '');
        }
      },
      (error: any) => {
        console.log(error);
        this.openSnackBar('Įvyko klaida. Kreipkitės į sistemos administratorių.', '');
      }
    );
  }

  openSnackBar(message: string, action: string) {
    this.loading.next(false);
    this.snackBar.open(message, action, {
      duration: 2500,
    });
  }
}
