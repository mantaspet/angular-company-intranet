import { Employee } from './../../employee.model';
import { EmployeeService } from './../../employee.service';
import { Subscription } from 'rxjs/Subscription';
import { DatePipe } from '@angular/common';
import { Component, Inject, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { NgForm } from '@angular/forms';
import { HolidayHistoryService } from '../holiday-history.service';
import { MatDialogRef } from '@angular/material';
import { MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-holiday-history-edit',
  templateUrl: './holiday-history-edit.component.html',
  styleUrls: ['./holiday-history-edit.component.css']
})
export class HolidayHistoryEditComponent implements OnInit, OnDestroy {
  editMode = false;
  wasChanged = false;
  employee_id: number;
  employeesSubscription: Subscription;
  employees: Employee[];
  filteredEmployees: Employee[];
  @ViewChild('f') employeeHistoryForm: NgForm;

  constructor(
    private employeeHistoryService: HolidayHistoryService,
    private datePipe: DatePipe,
    private employeeService: EmployeeService,
    public dialogRef: MatDialogRef<HolidayHistoryEditComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
    if (!this.data.managing) {
      this.getEmployeesList();
      this.employee_id = this.data.employee_id;
      if (this.data.editMode) {
        this.editMode = this.data.editMode;
        const tmpHolidayHistory = JSON.parse(JSON.stringify(this.data.employee_holiday_history));
        this.dialogRef.beforeClose().subscribe(
          () => {
            if (!this.wasChanged) {
              this.employeeHistoryForm.setValue({
                type: tmpHolidayHistory.type,
                from: tmpHolidayHistory.from,
                to: tmpHolidayHistory.to,
                responsible_employee: tmpHolidayHistory.responsible_employee
              });
            }
          }
        );
      }
    }
  }

  ngOnDestroy() {
    if (!this.data.managing) {
      this.employeesSubscription.unsubscribe();
    }
  }

  getEmployeesList() {
    this.employeesSubscription = this.employeeService.employeesChanged.subscribe(
      (response: any) => {
        this.employees = response;
        this.filteredEmployees = this.employees;
        console.log(this.employees);
      }
    );
    this.employeeService.getEmployees();
  }

  filterEmployees(name: string) {
    this.filteredEmployees = this.employees.filter((employee: Employee) =>
      employee.contact.last_name.toLowerCase().indexOf(name.toLowerCase()) === 0);
  }

  onSubmitHolidayHistory(form: NgForm, employee_holiday_history: any) {
    if (this.editMode) {
      this.alterDataStructure(form, employee_holiday_history);
      this.employeeHistoryService.updateHolidayHistory(employee_holiday_history);
      this.wasChanged = true;
    } else {
      this.alterDataStructure(form, employee_holiday_history);
      employee_holiday_history.employee_id = this.employee_id;
      this.employeeHistoryService.addHolidayHistory(employee_holiday_history);
      this.wasChanged = true;
    }
    this.editMode = false;
    this.dialogRef.close();
  }

  onConfirmRequest(is_confirmed: boolean) {
    this.data.employee_holiday_history.is_confirmed = is_confirmed;
    this.employeeHistoryService.confirmHolidayHistory(this.data.employee_holiday_history);
    this.dialogRef.close();
  }

  alterDataStructure(form: NgForm, employee_holiday_history: any) {
    this.datePipe.transform(employee_holiday_history.from, 'yyyy-MM-dd');
    this.datePipe.transform(employee_holiday_history.to, 'yyyy-MM-dd');
    if (!this.isValidDate(employee_holiday_history.from.toString())) {
      const day = employee_holiday_history.from.getDate();
      const month = employee_holiday_history.from.getMonth() + 1;
      const year = employee_holiday_history.from.getFullYear();
      employee_holiday_history.from = year + '-' + month + '-' + day;
    }
    if (!this.isValidDate(employee_holiday_history.to.toString())) {
      const day = employee_holiday_history.to.getDate();
      const month = employee_holiday_history.to.getMonth() + 1;
      const year = employee_holiday_history.to.getFullYear();
      employee_holiday_history.to = year + '-' + month + '-' + day;
    }
    return employee_holiday_history;
  }

  isValidDate(dateString: string) {
    const regEx = /^\d{4}-\d{2}-\d{2}$/;
    if (!dateString.match(regEx)) {
      return false;  // Invalid format
    }
    const d = new Date(dateString);
    if (!d.getTime()) {
      return false; // Invalid date (or this could be epoch)
    }
    return d.toISOString().slice(0, 10) === dateString;
  }

  displayFn(employee: Employee): string {
    if (employee !== null) {
      return employee.contact.first_name !== '' ? employee.contact.first_name + ' ' + employee.contact.last_name : '';
    }
    return '';
  }

  onClear() {
    this.employeeHistoryForm.reset();
  }

}
