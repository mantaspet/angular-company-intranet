import { MatSnackBar } from '@angular/material';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HolidayHistory } from './holiday-history.model';
import { Subject } from 'rxjs/Subject';
import { apiPath } from '../../../environments/environment';

@Injectable()
export class HolidayHistoryService {
  employee_holiday_history: HolidayHistory[];
  employeeHistoryChanged = new Subject<HolidayHistory[]>();
  loading = new Subject<boolean>();

  constructor(
    private httpClient: HttpClient,
    private snackBar: MatSnackBar
  ) { }

  getHolidayHistory(employee_id: number) {
    this.loading.next(true);
    this.httpClient.get(apiPath + '/employee-holiday/' + employee_id, {}).subscribe(
      (response: any) => {
        this.employee_holiday_history = response.item;
        console.log(response);
        this.employeeHistoryChanged.next(this.employee_holiday_history);
        this.loading.next(false);
      },
      (error: any) => {
        console.log(error);
        this.openSnackBar('Įvyko klaida. Kreipkitės į sistemos administratorių.', '');
      }
    );
  }

  getHolidayRequests() {
    this.loading.next(true);
    this.httpClient.get(apiPath + '/employee-holiday/requests').subscribe(
      (response: any) => {
        this.employee_holiday_history = response.items.data;
        console.log(response);
        this.employeeHistoryChanged.next(this.employee_holiday_history);
        this.loading.next(false);
      },
      (error: any) => {
        console.log(error);
        this.openSnackBar('Įvyko klaida. Kreipkitės į sistemos administratorių.', '');
      }
    );
  }

  getMyHolidayHistory() {
    this.loading.next(true);
    this.httpClient.get(apiPath + '/employee-holiday/my-requests').subscribe(
      (response: any) => {
        this.employee_holiday_history = response.items.data;
        console.log(response);
        this.employeeHistoryChanged.next(this.employee_holiday_history);
        this.loading.next(false);
      },
      (error: any) => {
        console.log(error);
        this.openSnackBar('Įvyko klaida. Kreipkitės į sistemos administratorių.', '');
      }
    );
  }

  addHolidayHistory(employee_holiday_history: HolidayHistory) {
    this.loading.next(true);
    employee_holiday_history.responsible_employee_id = employee_holiday_history.responsible_employee.id;
    this.httpClient.post(apiPath + '/employee-holiday', employee_holiday_history).subscribe(
      (response: any) => {
        if (this.employee_holiday_history) {
          this.employee_holiday_history.push(response.item);
          this.employeeHistoryChanged.next(this.employee_holiday_history);
        }
        this.openSnackBar('Atostogų prašymas pateiktas.', '');
      },
      (error: any) => {
        console.log(error);
        this.openSnackBar('Įvyko klaida. Kreipkitės į sistemos administratorių.', '');
      }
    );
  }

  updateHolidayHistory(employee_holiday_history: HolidayHistory) {
    this.loading.next(true);
    console.log(employee_holiday_history);
    employee_holiday_history.responsible_employee_id = employee_holiday_history.responsible_employee.id;
    this.httpClient.put(apiPath + '/employee-holiday/' + employee_holiday_history.id, employee_holiday_history).subscribe(
      () => {
        this.openSnackBar('Atostogų prašymas atnaujintas.', '');
      },
      (error: any) => {
        console.log(error);
        this.openSnackBar('Įvyko klaida. Kreipkitės į sistemos administratorių.', '');
      }
    );
  }

  confirmHolidayHistory(employee_holiday_history: HolidayHistory) {
    this.loading.next(true);
    console.log(employee_holiday_history);
    employee_holiday_history.responsible_employee_id = employee_holiday_history.responsible_employee.id;
    this.httpClient.put(apiPath + '/employee-holiday/' + employee_holiday_history.id, employee_holiday_history).subscribe(
      () => {
        const index = this.employee_holiday_history.indexOf(employee_holiday_history);
        if (index >= 0) {
          this.employee_holiday_history.splice(index, 1);
          this.employeeHistoryChanged.next(this.employee_holiday_history);
          this.openSnackBar('Prašymas patvirtintas.', '');
        }
      },
      (error: any) => {
        console.log(error);
        this.openSnackBar('Įvyko klaida. Kreipkitės į sistemos administratorių.', '');
      }
    );
  }

  openSnackBar(message: string, action: string) {
    this.loading.next(false);
    this.snackBar.open(message, action, {
      duration: 2500,
    });
  }
}
