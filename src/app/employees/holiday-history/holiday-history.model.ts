import { Employee } from '../employee.model';

export class HolidayHistory {
  id: number;
  is_confirmed: boolean;
  from: Date;
  to: Date;
  employee_id: number;
  type: string;
  responsible_employee_id: number;
  confirmed_by: boolean;

  responsible_employee: Employee;
}
