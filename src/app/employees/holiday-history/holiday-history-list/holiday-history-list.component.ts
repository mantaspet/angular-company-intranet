import { Component, OnInit } from '@angular/core';
import { DataSource } from '@angular/cdk/collections';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import { HolidayHistory } from '../holiday-history.model';
import { HolidayHistoryService } from '../holiday-history.service';
import { MatDialog } from '@angular/material';
import { HolidayHistoryEditComponent } from '../holiday-history-edit/holiday-history-edit.component';
import { ActivatedRoute, Params } from '@angular/router';

let employeeHistoryObservable;

@Component({
  selector: 'app-holiday-history-list',
  styleUrls: ['holiday-history-list.component.css'],
  templateUrl: 'holiday-history-list.component.html',
})
export class HolidayHistoryListComponent implements OnInit {
  employee_id: number;
  first_name: string;
  last_name: string;
  managing: boolean;
  my_history: boolean;
  displayedColumns = ['submitted_by', 'type', 'from', 'to', 'responsible_employee', 'confirmed_by', 'is_confirmed'];
  dataSource = new HolidayHistoryDataSource();
  loading = false;
  page: string;

  constructor(
    private employeeHistoryService: HolidayHistoryService,
    private route: ActivatedRoute,
    public dialog: MatDialog) {}

  ngOnInit() {
    this.employeeHistoryService.loading.subscribe(
      (status: boolean) => {
        this.loading = status;
      }
    );
    this.route.params.subscribe(
      (params: Params) => {
        this.employee_id = +params['id'];
        this.first_name = params['first_name'];
        this.last_name = params['last_name'];
        this.managing = (params['requests'] === 'requests');
        this.my_history = (params['my_holiday_requests'] === 'my_holiday_requests');
        employeeHistoryObservable = this.employeeHistoryService.employeeHistoryChanged;
        if (this.managing) {
          this.displayedColumns = ['submitted_by', 'type', 'from', 'to', 'responsible_employee'];
          this.employeeHistoryService.getHolidayRequests();
        } else if (!this.my_history) {
          this.displayedColumns = ['type', 'from', 'to', 'responsible_employee', 'confirmed_by'];
          this.employeeHistoryService.getHolidayHistory(this.employee_id);
        } else {
          this.displayedColumns = ['type', 'from', 'to', 'responsible_employee', 'is_confirmed', 'confirmed_by'];
          this.employeeHistoryService.getMyHolidayHistory();
        }
      }
    );
  }

  onNewHolidayHistory(): void {
    const employeeHistoryDialog = this.dialog.open(HolidayHistoryEditComponent, {
      data: {
        editMode: false,
        employee_id: this.employee_id,
        managing: false,
        my_history: this.my_history,
        employee_holiday_history: {
          type: '',
          from: '',
          to: '',
          responsible_employee: {
            contact: {
              first_name: '',
              last_name: ''
            }
          }
        }
      }
    });
  }

  onUpdateHolidayHistory(employee_holiday_history: HolidayHistory) {
    const employeeHistoryDialog = this.dialog.open(HolidayHistoryEditComponent, {
      data: {
        editMode: true,
        my_history: this.my_history,
        managing: this.managing,
        employee_id: this.employee_id,
        employee_holiday_history: employee_holiday_history
      }
    });
  }
}

export class HolidayHistoryDataSource extends DataSource<any> {
  connect(): Observable<HolidayHistory[]> {
    return employeeHistoryObservable;
  }

  disconnect() {}
}
