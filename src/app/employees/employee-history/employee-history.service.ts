import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { EmployeeHistory } from './employee-history.model';
import { Subject } from 'rxjs/Subject';
import { apiPath } from '../../../environments/environment';
import { MatSnackBar } from '@angular/material';

@Injectable()
export class EmployeeHistoryService {
  employee_history: EmployeeHistory[];
  employeeHistoryChanged = new Subject<EmployeeHistory[]>();
  loading = new Subject<boolean>();

  constructor(
    private httpClient: HttpClient,
    private snackBar: MatSnackBar) { }

  getEmployeeHistory(employee_id: number) {
    this.loading.next(true);
    this.httpClient.get(apiPath + '/employee-history/' + employee_id, {}).subscribe(
      (response: any) => {
        this.employee_history = response.item;
        this.employeeHistoryChanged.next(this.employee_history);
        this.loading.next(false);
      },
      (error: any) => {
        console.log(error);
        this.openSnackBar('Įvyko klaida. Kreipkitės į sistemos administratorių.', '');
      }
    );
  }

  getMyEmploymentHistory() {
    this.loading.next(true);
    this.httpClient.get(apiPath + '/my-employment-history').subscribe(
      (response: any) => {
        console.log(response);
        this.employee_history = response.item;
        this.employeeHistoryChanged.next(this.employee_history);
        this.loading.next(false);
      },
      (error: any) => {
        console.log(error);
        this.openSnackBar('Įvyko klaida. Kreipkitės į sistemos administratorių.', '');
      }
    );
  }

  addEmployeeHistory(employee_history: EmployeeHistory) {
    this.loading.next(true);
    this.httpClient.post(apiPath + '/employee-history', employee_history).subscribe(
      (response: any) => {
        this.employee_history.push(response.item);
        this.employeeHistoryChanged.next(this.employee_history);
        this.openSnackBar('Darbo istorija sukurta.', '');
      },
      (error: any) => {
        console.log(error);
        this.openSnackBar('Įvyko klaida. Kreipkitės į sistemos administratorių.', '');
      }
    );
  }

  updateEmployeeHistory(employee_history: EmployeeHistory) {
    this.loading.next(true);
    this.httpClient.put(apiPath + '/employee-history/' + employee_history.id, employee_history).subscribe(
      () => {
        this.openSnackBar('Darbo istorija atnaujinta.', '');
      },
      (error: any) => {
        console.log(error);
        this.openSnackBar('Įvyko klaida. Kreipkitės į sistemos administratorių.', '');
      }
    );
  }

  openSnackBar(message: string, action: string) {
    this.loading.next(false);
    this.snackBar.open(message, action, {
      duration: 2500,
    });
  }
}
