import { Position } from '../../positions/position.model';

export class EmployeeHistory {
    id: number;
    from: Date;
    to: Date;
    position_id: number;
    employee_id: number;

    position: Position;
}
