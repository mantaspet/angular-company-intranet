import { Position } from './../../../positions/position.model';
import { Subscription } from 'rxjs/Subscription';
import { DatePipe } from '@angular/common';
import { Component, Inject, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { NgForm } from '@angular/forms';
import { EmployeeHistoryService } from '../employee-history.service';
import { MatDialogRef } from '@angular/material';
import { MAT_DIALOG_DATA } from '@angular/material';
import { PositionService } from '../../../positions/position.service';

@Component({
  selector: 'app-employee-history-edit',
  templateUrl: './employee-history-edit.component.html',
  styleUrls: ['./employee-history-edit.component.css']
})
export class EmployeeHistoryEditComponent implements OnInit, OnDestroy {
  editMode = false;
  wasChanged = false;
  employee_id: number;
  positionsSubscription: Subscription;
  positions: Position[];
  filteredPositions: Position[];
  @ViewChild('f') employeeHistoryForm: NgForm;

  constructor(
    private employeeHistoryService: EmployeeHistoryService,
    private datePipe: DatePipe,
    private positionService: PositionService,
    public dialogRef: MatDialogRef<EmployeeHistoryEditComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
    this.getPositionsList();
    this.employee_id = this.data.employee_id;
    if (this.data.editMode) {
      this.editMode = this.data.editMode;
      const tmpEmployeeHistory = JSON.parse(JSON.stringify(this.data.employee_history));
      this.dialogRef.beforeClose().subscribe(
        () => {
          if (!this.wasChanged) {
            this.employeeHistoryForm.setValue({
              position: tmpEmployeeHistory.position.name,
              from: tmpEmployeeHistory.from,
              to: tmpEmployeeHistory.to
            });
          }
        }
      );
    }
  }

  ngOnDestroy() {
    this.positionsSubscription.unsubscribe();
  }

  getPositionsList() {
    this.positionsSubscription = this.positionService.positionsChanged.subscribe(
      (response: any) => {
        this.positions = response;
        this.filteredPositions = this.positions;
      }
    );
    this.positionService.getPositions();
  }

  filterPositions(name: string) {
    this.filteredPositions = this.positions.filter((position: Position) =>
      position.name.toLowerCase().indexOf(name.toLowerCase()) === 0);
  }

  onSubmitEmployeeHistory(form: NgForm, employee_history: any) {
    if (this.editMode) {
      this.alterDataStructure(form, employee_history);
      this.employeeHistoryService.updateEmployeeHistory(employee_history);
      this.wasChanged = true;
    } else {
      this.alterDataStructure(form, employee_history);
      employee_history.employee_id = this.employee_id;
      this.employeeHistoryService.addEmployeeHistory(employee_history);
      this.wasChanged = true;
    }
    this.editMode = false;
    this.dialogRef.close();
  }

  alterDataStructure(form: NgForm, employee_history: any) {
    employee_history.position_id = this.positions.find(position => position.name === form.value.position).id;
    this.datePipe.transform(employee_history.from, 'yyyy-MM-dd');
    this.datePipe.transform(employee_history.to, 'yyyy-MM-dd');
    if (!this.isValidDate(employee_history.from.toString())) {
      const day = employee_history.from.getDate();
      const month = employee_history.from.getMonth() + 1;
      const year = employee_history.from.getFullYear();
      employee_history.from = year + '-' + month + '-' + day;
    }
    if (employee_history.to && !this.isValidDate(employee_history.to.toString())) {
      const day = employee_history.to.getDate();
      const month = employee_history.to.getMonth() + 1;
      const year = employee_history.to.getFullYear();
      employee_history.to = year + '-' + month + '-' + day;
    }
    return employee_history;
  }

  isValidDate(dateString: string) {
    const regEx = /^\d{4}-\d{2}-\d{2}$/;
    if (!dateString.match(regEx)) {
      return false;  // Invalid format
    }
    const d = new Date(dateString);
    if (!d.getTime()) {
      return false; // Invalid date (or this could be epoch)
    }
    return d.toISOString().slice(0, 10) === dateString;
  }

  onClear() {
    this.employeeHistoryForm.reset();
  }

}
