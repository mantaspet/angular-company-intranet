import { Component, OnInit } from '@angular/core';
import { DataSource } from '@angular/cdk/collections';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import { EmployeeHistory } from '../employee-history.model';
import { EmployeeHistoryService } from '../employee-history.service';
import { MatDialog } from '@angular/material';
import { EmployeeHistoryEditComponent } from '../employee-history-edit/employee-history-edit.component';
import { ActivatedRoute, Params } from '@angular/router';

let employeeHistoryObservable;

@Component({
  selector: 'app-employee-history-list',
  styleUrls: ['employee-history-list.component.css'],
  templateUrl: 'employee-history-list.component.html',
})
export class EmployeeHistoryListComponent implements OnInit {
  employee_id: number;
  first_name: string;
  last_name: string;
  displayedColumns = ['position', 'from', 'to'];
  dataSource = new EmployeeHistoryDataSource();
  loading = false;
  my_history = false;

  constructor(
    private employeeHistoryService: EmployeeHistoryService,
    private route: ActivatedRoute,
    public dialog: MatDialog) {}

  ngOnInit() {
    this.employeeHistoryService.loading.subscribe(
      (status: boolean) => {
        this.loading = status;
      }
    );
    employeeHistoryObservable = this.employeeHistoryService.employeeHistoryChanged;
    this.route.params.subscribe(
      (params: Params) => {
        if (params['page'] === 'my_history') {
          this.my_history = true;
          this.employeeHistoryService.getMyEmploymentHistory();
        } else {
          this.employee_id = +params['id'];
          this.first_name = params['first_name'];
          this.last_name = params['last_name'];
          this.employeeHistoryService.getEmployeeHistory(this.employee_id);
        }
      }
    );
  }

  onNewEmployeeHistory(): void {
    const employeeHistoryDialog = this.dialog.open(EmployeeHistoryEditComponent, {
      data: {
        editMode: false,
        employee_id: this.employee_id,
        employee_history: {
          position: {
            name: ''
          },
          from: '',
          to: ''
        }
      }
    });
  }

  onUpdateEmployeeHistory(employee_history: EmployeeHistory) {
    const employeeHistoryDialog = this.dialog.open(EmployeeHistoryEditComponent, {
      data: {
        editMode: true,
        my_history: this.my_history,
        employee_id: this.employee_id,
        employee_history: employee_history
      }
    });
  }
}

export class EmployeeHistoryDataSource extends DataSource<any> {
  connect(): Observable<EmployeeHistory[]> {
    return employeeHistoryObservable;
  }

  disconnect() {}
}
