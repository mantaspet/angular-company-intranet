import { EmployeeHistory } from './employee-history/employee-history.model';
import { Department } from './../departments/department.model';
import { Office } from '../offices/office.model';
import { Contact } from './contact.model';
import { SalaryHistory } from './salary-history/salary-history.model';
import { HolidayHistory } from './holiday-history/holiday-history.model';
import { User } from '../authentication/user.model';

export class Employee {
  id: number;
  contact_id: number;
  office_id: number;
  department_id: number;
  position_id: number;
  user_id: number;

  contact: Contact;
  office: Office;
  department: Department;
  user: User;
  employee_history: EmployeeHistory[];
  employee_salary_history: SalaryHistory[];
  employee_holiday_history: HolidayHistory[];
}
