import { EmployeeHistoryService } from './employee-history/employee-history.service';
import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { EmployeeEditComponent } from './employee-edit/employee-edit.component';
import { FormsModule } from '@angular/forms';
import { EmployeeService } from './employee.service';
import { EmployeeListComponent } from './employee-list/employee-list.component';
import {
  MatTableModule, MatToolbarModule, MatButtonModule, MatInputModule, MatFormFieldModule, MatCardModule, MatDialogModule,
  MatSnackBarModule, MatIconModule, MatRadioModule, MatSelectModule, MatDatepickerModule, MatNativeDateModule, MatAutocompleteModule,
  MatCheckboxModule, MatMenuModule, MatTooltipModule, MatProgressBarModule
} from '@angular/material';
import { SalaryHistoryListComponent } from './salary-history/salary-history-list/salary-history-list.component';
import { SalaryHistoryEditComponent } from './salary-history/salary-history-edit/salary-history-edit.component';
import { SalaryHistoryService } from './salary-history/salary-history.service';
import { EmployeeHistoryListComponent } from './employee-history/employee-history-list/employee-history-list.component';
import { EmployeeHistoryEditComponent } from './employee-history/employee-history-edit/employee-history-edit.component';
import { HolidayHistoryListComponent } from './holiday-history/holiday-history-list/holiday-history-list.component';
import { HolidayHistoryEditComponent } from './holiday-history/holiday-history-edit/holiday-history-edit.component';
import { HolidayHistoryService } from './holiday-history/holiday-history.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatCardModule,
    MatButtonModule,
    MatTableModule,
    MatToolbarModule,
    MatDialogModule,
    MatSnackBarModule,
    MatIconModule,
    MatRadioModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatAutocompleteModule,
    MatCheckboxModule,
    MatMenuModule,
    MatTooltipModule,
    MatProgressBarModule
  ],
  declarations: [
    EmployeeEditComponent,
    EmployeeListComponent,
    SalaryHistoryListComponent,
    SalaryHistoryEditComponent,
    EmployeeHistoryListComponent,
    EmployeeHistoryEditComponent,
    HolidayHistoryListComponent,
    HolidayHistoryEditComponent
  ],
  providers: [EmployeeService, SalaryHistoryService, EmployeeHistoryService, HolidayHistoryService, DatePipe],
  entryComponents: [
    EmployeeEditComponent,
    SalaryHistoryEditComponent,
    EmployeeHistoryEditComponent,
    HolidayHistoryEditComponent,
  ]
})
export class EmployeesModule { }
