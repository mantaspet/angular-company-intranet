import { ConfirmBoxComponent } from './confirm-box/confirm-box.component';
import { TokenInterceptor } from './token.interceptor';
import { NgModule } from '@angular/core';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { MatDialogModule, MatButtonModule } from '@angular/material';
import { AuthorizationGuardService } from './authorization.guard';

@NgModule({
  declarations: [
    ConfirmBoxComponent
  ],
  imports: [
    MatButtonModule,
    MatDialogModule
  ],
  exports: [
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    },
    AuthorizationGuardService
  ]
})
export class SharedModule { }
