import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

@Injectable()
export class AuthorizationGuardService implements CanActivate {

  constructor(private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    const role = localStorage.getItem('role');
    const roles = route.data['roles'] as Array<string>;
    if (roles.indexOf(role) < 0) {
      alert('Jūsų paskyra neturi teisės prieiti prie šio puslapio. Jei taip neturėtų būti, kreipkitės į sistemos administratorių.');
      return false;
    }
    return true;
  }

}
